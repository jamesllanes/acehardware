<?php
	session_start();
	require 'dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="css/cart.css">
	<title>Ace Hardware</title>
	<meta name="description" content="Write some words to describe your html page">
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="newhome.php"><img src="ace2.png" width="14%"></a>
				<div id="Login_Container1">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['customer_ID']))
							{
								echo "Welcome Guest!";?>
								<br><a href="index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['customer_ID']))
							{
								echo "Welcome<br><font color='red'><b>".$_SESSION['fullname']."</b></font>!";
								echo "<form action='logout.php' method='POST'><input type='submit' value='Logout' class='logout'/></form>";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
				<div id="Search">
					<form action="search.php" method="GET">
						<div id="Search_Content">
							<input type="text" id="search_box" name="search" placeholder="Search Product">
							<button type="submit" id="search_btn"><img src="css/images/search3.png" width="20px"></button>
						</div>
					</form>
				</div>
			</div>
				<div id="CustomerService_btn">
					<button class="button"><a href="customer_service.php"/>CUSTOMER SERVICE</a></button>
				</div>
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="dropdown">
				  <button class="dropbtn">Categories</button>
				  <div class="dropdown-content">
				    <a href="1Paints_and_Sundries/paints_and_sundries.php">Paints and Sundries</a>
				    <a href="2Tools/tools.php">Tools</a>
				    <a href="3Electrical/electrical.php">Electrical</a>
				    <a href="4Plumbing/plumbing.php">Plumbing</a>
				    <a href="5Home_Hardware/home_hardware.php">Home Hardware</a>
				    <a href="6Houseware/houseware.php">Houseware</a>
				    <a href="7Lawn/lawn.php">Lawn and Outdoor</a>
				    <a href="8Automotive/automotive.php">Automotive</a>
				    <a href="9Appliances/appliances.php">Small Appliances</a>
				    <a href="10Chemicals/chemicals.php">Chemicals and Batteries</a>
				    <a href="11Pets/pets.php">Pets</a>
				  </div>
				</div>
				<div class="tab">
				  <a href="sale_and_specials.php"><button class="tablinks">Sale & Specials</button></a>
				  <button class="tablinks" onclick="location.href='../wishlist.php?customer_ID=<?php echo $_SESSION["customer_ID"]?>&w=0'">Wishlist</button>
				  <a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><button class="tablinks">Careers</button></a>
				  <a href="about_us.php"><button class="tablinks">About Us</button></a>
				</div>
				<div class="cart_tab">
					<p>Cart: 0 Items</p>
				</div>
			</div>
		</div>
		<div id="Middle"> <!--breadcrumb-->
			<!--<div class="breadcrumbs">
				<p>My Cart</p>
			</div>-->
		</div>
		<div id="Center"> <!--category links-->
			<div class="prod_content">
				<?php
					$customer_ID=$_REQUEST['customer_ID'];
					/*$select_cart2="SELECT * FROM cart WHERE customer_ID = '$customer_ID'";
					$result_cart2=mysqli_query($connect,$select_cart2);
					if(mysqli_num_rows($result_cart2)>0)
					{
						while($row=mysqli_fetch_array($result_cart2))
						{
							$Prod_ID=$row['Prod_ID'];
							$Prod_Name=$row['Prod_Name'];
							$Price=$row['Price'];
							$Quantity=$row['Quantity'];
							$Total=$row['Total'];

							//insert items to purchase table
							$insert_purchase="INSERT INTO purchases (Prod_ID, Prod_Name, Price, Quantity, Total, customer_ID) VALUES ('$Prod_ID', '$Prod_Name', '$Price', '$Quantity', '$Total', $customer_ID')";
							$result_insert=mysqli_query($connect,$insert_purchase);
						}
					}*/

					$insert_purchase="INSERT INTO purchases (Prod_ID, Prod_Name, Price, Quantity, Total, customer_ID) SELECT Prod_ID, Prod_Name, Price, Quantity, Total, customer_ID FROM cart WHERE customer_ID = '$customer_ID'";
					$result_insert=mysqli_query($connect,$insert_purchase);

					$select_purchase="SELECT * FROM cart WHERE customer_ID='$customer_ID'";
					$result_purchase=mysqli_query($connect,$select_purchase);
					$Grand_Total=0;
					echo "<table frame=box bordercolor='#8C001A' class='prod_list' style='width:750px; height: 358px;'>";
					echo "<tr><th colspan='4' class='title'>Purchases<br></th></tr>";
					

					if(mysqli_num_rows($result_purchase)>0)
					{
						while($row=mysqli_fetch_array($result_purchase))
						{
							$Prod_ID1=$row['Prod_ID'];
							//$_SESSION['Prod_ID1']=$Prod_ID1;
							$Prod_Name=$row['Prod_Name'];
							$Price=$row['Price'];
							$Quantity_update=$row['Quantity'];
							$Subtotal=$Price*$Quantity_update;
							$Grand_Total=$Grand_Total+$row['Total'];

							echo "<tr class='title'><td>Product Name</td><td>Price</td><td>Quantity</td><td>Subtotal</td></tr>";
							echo "<tr class='borderbottom1'><td>".$Prod_Name."</td><td>₱".$Price."</td><td>x".$Quantity_update."</td><td>₱".$Subtotal."</td></tr>";

							$select_sales="SELECT * FROM sales";
							$result_sales=mysqli_query($connect,$select_sales);
							if(mysqli_num_rows($result_sales)>0)
							{
								while($row=mysqli_fetch_array($result_sales))
								{
									$Prod_ID2=$row['Prod_ID'];
									if($Prod_ID1==$Prod_ID2)
									{
										$Quantity=$row['Quantity']+$Quantity_update;
										$update_qty="UPDATE sales SET Quantity ='$Quantity'";
										$result_qty=mysqli_query($connect,$update_qty);

										$Additional=$row['Total']+$Subtotal;
										$update_total="UPDATE sales SET Total ='$Additional'";
										$result_total=mysqli_query($connect,$update_total);

										//echo "update";
									}
								}
							}
							else
							{
								/*$select_purchase2="SELECT * FROM cart WHERE customer_ID='$customer_ID'";
								$result_purchase2=mysqli_query($connect,$select_purchase2);
								if(mysqli_num_rows($result_purchase2)>0)
								{
									while($row=mysqli_fetch_array($result_purchase2))
									{
										$Prod_ID1=$row['Prod_ID'];
										$Prod_Name=$row['Prod_Name'];
										$Price=$row['Price'];
										$Quantity_update=$row['Quantity'];
										$Subtotal=$Price*$Quantity_update;

										*/
										//$insert_sales="INSERT INTO sales (Prod_ID, Prod_Name, Price, Quantity, Total) VALUES ('$Prod_ID1', '$Prod_Name', '$Price', '$Quantity_update', '$Subtotal') WHERE Prod_ID = '$Prod_ID1'";
										$insert_sales="INSERT INTO sales (Prod_ID, Prod_Name, Price, Quantity, Total) SELECT Prod_ID, Prod_Name, Price, Quantity, Total FROM  cart WHERE customer_ID = '$customer_ID'";
										$result_insert2=mysqli_query($connect,$insert_sales);
										//echo "insert sales";
										//print_r($customer_ID);
									//}
								//}
							}

							$select_inv="SELECT * FROM inventory WHERE Prod_ID = '$Prod_ID1'";
							//print_r($_SESSION['Prod_ID1']);
							$result_inv=mysqli_query($connect,$select_inv);
							if(mysqli_num_rows($result_inv)>0)
							{
								while($row=mysqli_fetch_array($result_inv))
								{
									$Quantity=$row['Quantity']-$Quantity_update;
									$update_inventory="UPDATE inventory SET Quantity ='$Quantity' WHERE Prod_ID = '$Prod_ID1'";
									$result_update=mysqli_query($connect,$update_inventory);
								}
							}
						}
						echo "<tr class='borderbottom1'><td colspan='3' align= 'center'><b>Total</b></td><td>₱<b>".$Grand_Total."</b></td></tr>";
						echo "<tr align='center'><td colspan='4'>Thank you for purchasing!</td></tr>";
						echo "<tr align='center'><td colspan='4'><a href='newhome.php'><input type='button' class='button' value='Back'></a></td></tr>";
						echo "</table>";
						echo "</form>";
					}
					else
					{
						?><tr class='no_border'><td colspan='4' align='center'>Cart is empty.</td></tr><?php
					}	

					$delete_cart="DELETE FROM cart WHERE customer_ID = '$customer_ID'";
					$result_delete=mysqli_query($connect,$delete_cart);
					?>
			</div>
		</div>
	</div>
</body>
</html>