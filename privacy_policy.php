<?php
	session_start();
	require 'dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="css/cart.css">
	<link rel="icon" href="ace_logo2.png" type="image/icon type">
	<title>Ace Hardware</title>
	<meta name="description" content="Write some words to describe your html page">
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="newhome.php"><img src="ace2.png" width="14%"></a>
				<div id="Login_Container1">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['customer_ID']))
							{
								echo "Welcome Guest!";?>
								<br><a href="../index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['customer_ID']))
							{
								echo "Welcome<br><font color='red'><b>".$_SESSION['fullname']."</b></font>!";
								echo "<form action='../logout.php' method='POST'><input type='submit' value='Logout' class='logout'/></form>";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
				<div id="Search">
					<form action="search.php" method="GET">
						<div id="Search_Content">
							<input type="text" id="search_box" name="search" placeholder="Search Product">
							<button type="submit" id="search_btn"><img src="css/images/search3.png" width="20px"></button>
						</div>
					</form>
				</div>
			</div>
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="dropdown">
				  <button class="dropbtn">Categories</button>
				  <div class="dropdown-content">
				    <a href="../1Paints_and_Sundries/paints_and_sundries.php">Paints and Sundries</a>
				    <a href="../2Tools/tools.php">Tools</a>
				    <a href="../3Electrical/electrical.php">Electrical</a>
				    <a href="../4Plumbing/plumbing.php">Plumbing</a>
				    <a href="../5Home_Hardware/home_hardware.php">Home Hardware</a>
				    <a href="../6Houseware/houseware.php">Houseware</a>
				    <a href="../7Lawn/lawn.php">Lawn and Outdoor</a>
				    <a href="../8Automotive/automotive.php">Automotive</a>
				    <a href="../9Appliances/appliances.php">Small Appliances</a>
				    <a href="../10Chemicals/chemicals.php">Chemicals and Batteries</a>
				    <a href="../11Pets/pets.php">Pets</a>
				  </div>
				</div>
				<div class="tab">
				  <a href="sale_and_specials.php"><button class="tablinks">Sale & Specials</button></a>
				  <button class="tablinks" onclick="location.href='../wishlist.php?customer_ID=<?php echo $_SESSION["customer_ID"]?>&w=0'">Wishlist</button>
				  <a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><button class="tablinks">Careers</button></a>
				  <a href="about_us.php"><button class="tablinks">About Us</button></a>
				</div>
				<div class="cart_tab">
					<?php
						if(empty($_SESSION['customer_ID']))
						{
							echo "<p><a href='cart_with_css.php'><font color='#FFFFFF'>Cart: 0 Item(s)</a></font></p>";
						}
						elseif(!empty($_SESSION['customer_ID']))
						{
							$select_cart="SELECT * FROM cart WHERE customer_ID='$_SESSION[customer_ID]'";
							$result=mysqli_query($connect,$select_cart);
							$cart_count=mysqli_num_rows($result);
						
							echo "<p><a href='cart_with_css.php'><font color='#FFFFFF'>Cart: ".$cart_count." Item(s)</a></font></p>"; 
						}
						else
						{
							echo ' ';
						}   
					?>
				</div>
			</div>
		</div>
		<div id="Center"> <!--category links-->
			<div class="prod_content">
				<div class="privacy-policy">
					<table>
						<tr>
							<td>
								<h1>Privacy Policy</h1>
								<p>
									Ace Hardware Philippines Inc. ("Ace Hardware" or "We" or "Us") respects your privacy and values the relationship we have with you. We have created this Privacy Policy to inform you of the types of information that we collect from you, whether in-store, online or via one of our mobile applications and how we use and with whom we share said information. This Privacy Policy also describes the security measures we undertake to protect your information and how you can communicate with us regarding our privacy practices
								</p>
								<p><u><b>INFORMATION WE COLLECT/YOU PROVIDE</b></u></p>
								<p>
									The information that we collect are only those necessary and relevant to our dealings with you. Said information will be kept for as long as they are pertinent for the purpose for which they were collected and/or as we are required under the law.
								</p>
								<p>
									We collect and store information that you provide in-store, online or via one of our mobile applications , such as but not limited to, when you create or manage an Ace Hardware account or an Ace Rewards membership account, when you place an order, when you sign up to receive emails, newsletters or other promotional communications, when you contact us with online inquiries and/or when you otherwise voluntarily submit or provide information to us. When you undertake any of the said actions, the personal information we collect from you may include your:
								</p>
								<p>
									- Name
									- Delivery Address
									- Email Address
									- Contact Number
									- Mobile Number
									- Date of Birth
									- Gender
									- Ace Rewards Card information
									- Credit Card numbers and expiration dates
									- Account and log-in IDs, passwords
									- Information regarding orders
									- Information regarding your interests and preferences.
								</p>
								<p>
									The personal information that we collect are those you have voluntarily submitted to us. If you provide personal information of any third party to us, we assume that you have obtained the required consent from the relevant third party to share and transfer his/her personal information and to allow us, our agents and third party service providers to process said information in accordance with this Privacy Policy. If you choose to withdraw the consent for the collection and use of personal information, we may not be able to provide you with our services.
								</p>
								<p>
									If you create or link your Ace Hardware account with any of your social media account, we may access the information you provided to the social media provider and process said information in accordance with this Privacy Policy.
								</p>
								<p><u><b>TECHNOLOGY-COLLECTED INFORMATION</b></u></p>
								<p>
									We use technologies, such cookies, web server logs and web beacons, to collect and store certain information whenever you interact, access or use any of our online services, visit our website, view or click on our online advertisements (including our advertisements on third party websites), visit our social media pages or download and use one of our mobile applications. For example, we collect your IP address, browser and device information, including operating system, application software and reference site domain name, every time you access or use the online services. We also collect information regarding customer traffic patterns and usage and mobile application usage data.
								</p>
								<p>
									Cookies are small data files created by a web server and stored by your browser. They help us to identify account holders and Ace Rewards members and to optimize the log-in, account management, and shopping experience. Cookies also allow us to hold selections in a shopping cart when a user leaves without checking out. Most browsers accept cookies automatically but allow you to disable them. If you disable cookies, you may not be able to use some features of the online services. To learn more about cookies and how to disable them, please consult the "Help" feature of your browser or your device's user guide.
								</p>
								<p>
									Our web servers may log details such as your browser type, operating system type and domain and other system settings. The web server logs may also record information, such as the IP address of the device you use to connect to the internet and the address of the web page that linked you to our website. We may place tags on our web pages called “web beacons” to control which web servers collect this information.
								</p>
								<p><u><b>INFORMATION WE SHARE/TRANSFER AND WITH WHOM WE SHARE/TRANSFER IT</b></u></p>
								<p>
									We do not sell or otherwise disclose or transfer personal information we collect about you, except as described in this Privacy Policy. We may share, transfer or disclose your personal information with:
								</p>
								<ul>
									<li>Our branches and affiliates.</li>
									<li>
										Third party service providers who perform services based on our instructions. These service providers are authorized to use or disclose your personal information only as may be necessary to perform services on our behalf or to comply with legal requirements. Examples of these service providers include entities that provide web hosting, advertising and marketing service, those that process credit card payments, those that operate our online services, manage our customer accounts and Ace Rewards member accounts, those that provide fraud protection and credit risk reduction and those engaged in courier and delivery services.
									</li>
									<li>Other third parties with your consent</li>
									<li>
										Other third parties as allowed and limited by Republic Act No. 10173, its implementing rules and regulations, other data privacy laws, and such issuances from the National Privacy Commission
									</li>
								</ul>
								<p>
									We reserve the right to transfer your personal information in the event we sell or transfer all or a portion of our business or assets. We will use reasonable efforts to direct the transferee to use and process your personal information in a manner that is consistent with this Privacy Policy.
								</p>
								<p>
									We may also disclose information that is necessary to comply with any applicable law, regulation, legal process or governmental request. In addition, we may disclose any information when it is necessary to prevent physical harm or financial loss or in connection with suspected or actual illegal activity.
								</p>
								<p><u><b>HOW WE USE THE INFORMATION</b></u></p>
								<p>We may use the information that we collected or that you provided to:</p>
								<ul>
									<li>Process your payment transactions</li>
									<li>Create and manage your online account, including access to your online and in-store purchase history</li>
									<li>Manage your Ace Rewards membership account</li>
									<li>Improve our online services and provide other services to you for a more fulfilling shopping experience</li>
									<li>Respond to your inquiries</li>
									<li>Send you promotional materials or other communications (i.e contests, special events, etc.)</li>
									<li>Evaluate and improve our business, such as but not limited to performing data analytics and auditing and other internal functions</li>
									<li>Comply with applicable legal requirements, relevant industry standards and our policies</li>
								</ul>
								<p>
									We also may use your personal information in other ways for which we provide specific notice at the time of collection.
								</p>
								<p><u><b>PROTECTION OF INFORMATION</b></u></p>
								<p>
									We have in place appropriate administrative, technical and physical security measures and safeguards designed to protect the personal information you provide against accidental, unlawful or unauthorized destruction, loss, alteration, access, disclosure or use. If you have any questions about security at our website, please send an e-mail to dpo.acehardware@smretail.com.
								</p>
								<p><u><b>UPDATING YOUR INFORMATION</b></u></p>
								<p>
									The information you submit to us or to our authorized agent or third party service providers must be accurate and updated. You may review, change or request deletion of your personal information by sending an e-mail to dpo.acehardware@smretail.com.
								</p>
								<p>We reserve the right to request for documentation to verify the information provided by you.</p>
								<p><u><b>RIGHT TO OPT OUT FROM RECEIVING COMMUNICATION</b></u></p>
								<p>
									You may choose whether or not you would want to receive any communication from Ace Hardware. For e-mail communications, you may click the "unsubscribe" link at the bottom of each e-mail you receive from us. You may also send us an e-mail at dpo.acehardware@smretail.com to indicate your preference, such as to unsubscribe from receiving text messages or to stop receiving mail or phone communications
								</p>
								<p><u><b>ACCESSING LINKS TO OTHER SITES</b></u></p>
								<p>
									Our website may provide links to other websites, which may operate independently from us. Linked sites have their own privacy notices or policies. To the extent any linked websites you visit are not owned or operated by us, we are not responsible for the linked sites’ content, any use of the sites, or the privacy practices thereof.
								</p>
								<p>
									If you have accessed our online services through a link from our advertising or marketing partners, the information you provide to us through these framed Web pages is collected by us, and our use of this information is governed by this Privacy Policy.
								</p>
								<p>
									Our online services offer you the ability to interact with, and may allow you to share information, product reviews and other user generated content with social media platforms such as Facebook, Twitter, Google+, Pinterest, and YouTube, and social commerce networks, such as PowerReviews and Bazaarvoice. Please be advised that your sharing of information, product reviews or other user generated content, and any information gathered by these social media platforms and social commerce networks in connection with your sharing is not covered by this Privacy Policy, but will be governed by the privacy policy and terms of use of the applicable platform or network.
								</p>
								<p><u><b>DATA CONTROLLER/DATA PROCESSOR</b></u></p>
								<p>
									Ace Hardware will assume control and responsibility for the personal information that you provide to us. _________________ will process such information.
								</p>
								<p><u><b>CHANGES IN THE PRIVACY POLICY</b></u></p>
								<p>
									Our online Services will continue to evolve to bring you new features and services and to implement technological advances. As a result, we may change this Privacy Policy from time to time without prior notice. Revised versions of this Privacy Policy will be posted on this page, together with an updated effective date. In some cases, we may also send an email or other communication notifying users of the changes. You should check this page periodically to see if any recent changes to this Privacy Policy have occurred. By downloading, installing, accessing, or using any of our online services after we post any such changes, you agree to the terms of this Privacy Policy as modified.
								</p>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="footer.png" height="80px" width="200px"></br>
				<a href="customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	</div>
</body>
</html>