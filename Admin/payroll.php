<?php
	session_start();
	require '../dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="../Admin/css/admin.css">
	<title>Ace Hardware | Manage Accounts</title>
	<meta name="description" content="Write some words to describe your html page">
	<style>
	td{
		padding: 5px;
	}
	body{
		overflow: hidden;
	}
	</style>
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="dashboard.php"><img src="css/images/ace2.png" width="14%"></a>
				<div id="Login_Container">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['usertype']))
							{
								echo "Welcome Guest!";?>
								<br><a href="index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['usertype']))
							{
								echo "Welcome ".$_SESSION['usertype']." ".$_SESSION['fullname']."!";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
			</div>
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="menu_content">
					<a href="dashboard.php" style="text-decoration: none; color:white">Dashboard</a> > Manage Accounts
				</div>
			</div>
			<div id="Middle"> <!--Menu ICONS-->
				<div class="icons_grid">
					<div class="icons_two">
						<a href="manage_accts.php"><img src="css/images/icon-manage-accounts.png" height="90px"><p class="manage">Manage Accounts</p></a>
					</div>
					<div class="icons_four">
						<a href="../logout.php"><img src="css/images/icon-logout.png" height="90px"><p class="logout">Logout</p></a>
					</div>
				</div>
			</div>
		</div>
		<div id="Center"> <!--Menu Content-->
			<div class="sales_content">
			<table border="5" bordercolor="#57000c" class="nohover" style="width:750px">
			<?php
			$emp_id=$_REQUEST['emp_ID'];
			$query3="SELECT * FROM userinfotable_employee WHERE user_ID='$emp_id'";//employee
			$result3=mysqli_query($connect,$query3);
			$count3=mysqli_num_rows($result3);
			
			
			if($count3>0)//employee
			{
				while($row=mysqli_fetch_array($result3))
						{
							echo '<tr><td colspan="2"><b>Payroll</b></td></tr>';
							echo '<tr><td>Employee ID</td><td>'.$row['user_ID'].'</td></tr>';
							echo '<tr><td>Employee Name</td><td>'.$row['fullname'].'</td></tr>';
							echo '<tr><td>Rate/Hour</td><td>Php.40.00</td></tr>';
							echo '<tr><td>Rate/Day</td><td>6 hrs</td></tr>';
							echo '<tr><td>Pay/Day</td><td>Php.240.00</td></tr>';
							echo '<tr><td>Gross Pay</td><td>Php.5,760.00</td></tr>';
							echo '<tr><td>Net Pay</td><td>Php.5,600.00</td></tr>';
						}
			}
			
			?>
			</table>
			</br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br>
			<table border="5" bordercolor="#57000c" class="nohover" style="width:750px">
			<tr>
				<td colspan="2"><b>Deductions</b></td>
			<tr>
			<tr>
				<td>Philheath</td><td>Php.40.00</td>
			</tr>
			<tr>
				<td>SSS</td><td>Php.80.00</td>
			</tr>
			<tr>
				<td>Taxes</td><td>Php.40.00</td>
			</tr>
			<tr>
				<td><b>Total Deductions</b></td><td>Php. 160.00</td>
			</tr>

			</table>
			</br></br></br></br></br></br></br></br></br>
<button name="Back" class="all_btn"><a href="manage_accts.php">Back</a></button>			
			</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="../footer.png" height="80px" width="200px"></br>
				<a href="../customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="../terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="../privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="../1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="../2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="../3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="../4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="../5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="../6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="../7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="../8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="../9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="../10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="../11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="../cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="../fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="../logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	</div>
</body>
</html>