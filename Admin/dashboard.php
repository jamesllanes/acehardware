<?php
	session_start();
	require '../dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="../Admin/css/admin.css">
	<title>Ace Hardware | Dashboard</title>
	<meta name="description" content="Write some words to describe your html page">
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="dashboard.php"><img src="css/images/ace2.png" width="14%"></a>
				<div id="Login_Container">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['usertype']))
							{
								echo "Welcome Guest!";?>
								<br><a href="../index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['usertype']))
							{
								echo "Welcome ".$_SESSION['usertype']." ".$_SESSION['fullname']."!";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
			</div>
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="menu_content">
				Dashboard
				</div>
			</div>
			<div id="Middle"> <!--Menu ICONS-->`
				<div class="icons_grid">
					<?php
						if($_SESSION['usertype']=='Admin')
						{?>
							<div class="icons_one">
								<a href="sales.php"><img src="css/images/icon-sales.png" height="80px"><br><p>Sales</p></a>
							</div>
							<div class="icons_two">
								<a href="inventory.php"><img src="css/images/icon-inventory.png" height="80px"><br><p>Inventory</p></a>
							</div>
							<div class="icons_three">
								<a href="supplier.php"><img src="css/images/icon-supplier.png" height="80px"><br><p>Supplier</p></a>
							</div>
							<div class="icons_four">
								<a href="manage_accts.php"><img src="css/images/icon-manage-accounts.png" height="90px"><p class="manage">Manage Accounts</p></a>
							</div>
							<div class="icons_five">
								<a href="../logout.php"><img src="css/images/icon-logout.png" height="90px"><p class="logout">Logout</p></a>
							</div><?php
						}
						elseif($_SESSION['usertype']=='Employee')
						{?>
							<div class="icons_two">
								<a href="employee_inventory.php"><img src="css/images/icon-inventory.png" height="80px"><br><p>Inventory</p></a>
							</div>
							<div class="icons_three">
								<a href="employee_supplier.php"><img src="css/images/icon-supplier.png" height="80px"><br><p>Supplier</p></a>
							</div>
							<div class="icons_four">
								<a href="../logout.php"><img src="css/images/icon-logout.png" height="90px"><p class="logout">Logout</p></a>
							</div><?php
						}
						else
						{?>
							//invalid
							<script type="text/javascript"> alert("Invalid Username or Password!"); window.location = "../index.php"; </script><?php
						}
				?>
				</div>
			</div>
		</div>
		<div id="Center"> <!--Menu Content-->
			<div class="grid_list">
			HELLO
			</div>
		</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="../footer.png" height="80px" width="200px"></br>
				<a href="../customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="../terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="../privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="../1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="../2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="../3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="../4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="../5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="../6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="../7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="../8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="../9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="../10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="../11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="../cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="../fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="../logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	</div>
</body>
</html>