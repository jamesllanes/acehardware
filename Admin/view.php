<?php
	session_start();
	require '../dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="../Admin/css/admin.css">
	<title>Ace Hardware | Manage Accounts</title>
	<meta name="description" content="Write some words to describe your html page">
	<style>
	td{
		padding: 5px;
	}
	body{
		overflow: hidden;
	}
	</style>
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="dashboard.php"><img src="css/images/ace2.png" width="14%"></a>
				<div id="Login_Container">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['usertype']))
							{
								echo "Welcome Guest!";?>
								<br><a href="index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['usertype']))
							{
								echo "Welcome ".$_SESSION['usertype']." ".$_SESSION['fullname']."!";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
			</div>
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="menu_content">
					<a href="dashboard.php" style="text-decoration: none; color:white">Dashboard</a> > Manage Accounts
				</div>
			</div>
			<div id="Middle"> <!--Menu ICONS-->
				<div class="icons_grid">
					<div class="icons_two">
						<a href="manage_accts.php"><img src="css/images/icon-manage-accounts.png" height="90px"><p class="manage">Manage Accounts</p></a>
					</div>
					<div class="icons_four">
						<a href="../logout.php"><img src="css/images/icon-logout.png" height="90px"><p class="logout">Logout</p></a>
					</div>
				</div>
			</div>
		</div>
		<div id="Center"> <!--Menu Content-->
			<div class="sales_content">
			<img src="" height="300" width="300" align="center"></br></br>
			<table border="5" bordercolor="#57000c" class="nohover" style="width:750px">
			<?php
				$view=$_REQUEST['view_ID'];
					$_SESSION['view_ID']=$view;
					$view_ID=$_SESSION['view_ID'];
					
					$utype=$_REQUEST['utype'];

					
					$query1="SELECT * FROM userinfotable_admin WHERE user_ID='$view_ID' AND usertype='$utype'";//admin
					$query2="SELECT * FROM userinfotable_customer WHERE customer_ID='$view_ID' AND usertype='$utype'";//customer
					$query3="SELECT * FROM userinfotable_employee WHERE user_ID='$view_ID' AND usertype='$utype'";//employee

					$result1=mysqli_query($connect,$query1);
					$result2=mysqli_query($connect,$query2);
					$result3=mysqli_query($connect,$query3);

					$count1=mysqli_num_rows($result1);
					$count2=mysqli_num_rows($result2);
					$count3=mysqli_num_rows($result3);
					
					
					if($count1>0)//admin
					{
						?><form action="view.php?view_ID=<?php echo $view_ID?>" method="POST"><?php
						$query1="SELECT * FROM userinfotable_admin WHERE user_ID= '$view_ID'";//admin
						$result1=mysqli_query($connect,$query1);

						while($row=mysqli_fetch_array($result1))
						{?>
								<tr>
									<td></b>Admin ID:</b></td>
									<td><?php echo $row['user_ID'];?></td>
								</tr>
								<tr>
									<td></b>Fullname:</b></td>
									<td><?php echo $row['fullname'];?></td>
								</tr>
								<tr>
									<td></b>Gender:</b></td>
									<td><?php echo $row['gender'];?></td>
								</tr>
								<tr>
									<td></b>Email:</b></td>
									<td><?php echo $row['email'];?></td>
								</tr>
								<tr>
									<td></b>Username:</b></td>
									<td><?php echo $row['admin_username'];?></td>
								</tr>
								<tr>
									<td></b>Password:</b></td>
									<td><?php echo $row['admin_password'];?></td>
								</tr>
								<tr>
									<td>Usertype:</td>
									<td><?php echo $row['usertype'];?></td>
								</tr>
								<tr>
									<td colspan="2">
										<!--<input type="submit" class="all_btn" name="edit" value="Update">-->
										
										<?php echo "<button name='edit' class='all_btn'><a href='edit.php?edit_ID=$row[user_ID]'>Edit</button>&nbsp;";
								echo "<button name='delete' class='all_btn'><a href='delete.php?delete_ID=$row[user_ID]&delete_username=$row[admin_username]'>Delete</a></button>";
									?><button name="Back" class="all_btn"><a href="manage_accts.php">Back</a></button>
									</td>
								</tr>
							</form><?php
						}
					}
					elseif($count2>0)//customer
					{
						while($row=mysqli_fetch_array($result2))
						{?>
							<form action="view.php?view_ID=<?php echo $view_ID ?>" method="POST">
								<tr>
									<td></b>User ID:</b></td>
									<td><?php echo $row['customer_ID'];?></td>
								</tr>
								<tr>
									<td></b>Fullname:</b></td>
									<td><?php echo $row['fullname'];?></td>
								</tr>
								<tr>
									<td></b>Gender:</b></td>
									<td><?php echo $row['gender'];?></td>
								</tr>
								<tr>
									<td></b>Email:</b></td>
									<td><?php echo $row['email'];?></td>
								</tr>
								<tr>
									<td></b>Username:</b></td>
									<td><?php echo $row['customer_username'];?></td>
								</tr>
								<tr>
									<td></b>Password:</b></td>
									<td><?php echo $row['customer_password'];?></td>
								</tr>
								<tr>
									<td>Usertype:</td>
									<td><?php echo $row['usertype'];?></td>
								</tr>
								<tr>
									<td>
									<?php echo "<button name='edit' class='all_btn'><a href='edit.php?edit_ID=$row[user_ID]'>Edit</button>&nbsp;";
								echo "<button name='delete' class='all_btn'><a href='delete.php?delete_ID=$row[user_ID]&delete_username=$row[customer_username]'>Delete</a></button>";
									?>
									<button name="Back" class="all_btn"><a href="manage_accts.php">Back</a></button>
									</td>
									</tr>
									
							</form><?php
						}
					}
					elseif($count3>0)//employee
					{
						while($row=mysqli_fetch_array($result3))
						{?>
							<form action="view.php?view_ID=<?php echo $view_ID; ?>" method="POST">
								<tr>
									<td></b>User ID:</b></td>
									<td><?php echo $row['user_ID'];?></td>
								</tr>
								<tr>
									<td></b>Fullname:</b></td>
									<td><?php echo $row['fullname'];?></td>
								</tr>
								<tr>
									<td></b>Gender:</b></td>
									<td><?php echo $row['gender'];?></td>
								</tr>
								<tr>
									<td></b>Email:</b></td>
									<td><?php echo $row['email'];?></td>
								</tr>
								<tr>
									<td></b>Username:</b></td>
									<td><?php echo $row['employee_username'];?></td>
								</tr>
								<tr>
									<td></b>Password:</b></td>
									<td><?php echo $row['employee_password'];?></td>
								</tr>
								<tr>
									<td>Usertype:</td>
									<td><?php echo $row['usertype'];?></td>
								</tr>
								<tr>
								<?php
								echo "<td colspan='2'><button name='payroll' class='all_btn'><a href='payroll.php?emp_ID=$row[user_ID]'>Payroll</a></button>&nbsp;";
									 echo "<button name='edit' class='all_btn'><a href='edit.php?edit_ID=$row[user_ID]'>Edit</button>&nbsp;";
								echo "<button name='delete' class='all_btn'><a href='delete.php?delete_ID=$row[user_ID]&delete_username=$row[employee_username]'>Delete</a></button>";
								echo "</tr></table></form>";
								echo '</br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br>';
						echo '<table border="5" bordercolor="#57000c" class="nohover" style="width:750px">';
						echo '<tr><td colspan="2"><b>Schedule</b></td></tr>';
						echo '<tr><td>Mondays</td><td>1:00 - 6:00pm</td></tr>';
						echo '<tr><td>Wednesdays</td><td>2:00 - 7:00pm</td></tr>';
						echo '<tr><td>Fridays</td><td>12:00 - 5:00pm</td></tr>';
						echo "<tr><td colspan='2'><button name='changesched' class='all_btn'><a href='changesched.php?emp_ID=$row[user_ID]'>Request to Change Schedule</button>";
						echo '</table>';
									?>
									<button name="Back" class="all_btn"><a href="manage_accts.php">Back</a></button>
								

							<?php
							
						}
						
					}
			?>
			
			
			
			</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="../footer.png" height="80px" width="200px"></br>
				<a href="../customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="../terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="../privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="../1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="../2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="../3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="../4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="../5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="../6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="../7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="../8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="../9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="../10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="../11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="../cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="../fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="../logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	</div>
</body>
</html>