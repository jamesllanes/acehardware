<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="css/product_list.css">
	<title>Ace Hardware</title>
	<meta name="description" content="Write some words to describe your html page">
</head>
<body>
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="newhome.php"><img src="ace2.png" width="14%"></a>
				<div id="Login_Container">
					<div id="Login_Content">
						Welcome! <br> Create account | Signin
					</div>
				</div>
				<div id="Search">
					<form action="search.php" method="GET">
						<div id="Search_Content">
							<input type="text" id="search_box" name="search" placeholder="Search Product">
							<button type="submit" id="search_btn"><img src="search3.png" width="20px"></button>
						</div>
					</form>
				</div>
			</div>
				<div id="CustomerService_btn">
					<button class="button"><a href="customer_service.php"/>CUSTOMER SERVICE</a></button>
				</div>

			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="dropdown">
				  <button class="dropbtn">Categories</button>
				  <div class="dropdown-content">
				    <a href="1paint.php">Paints and Sundries</a>
				    <a href="2tools.php">Tools</a>
				    <a href="3electrical.php">Electrical</a>
				    <a href="4plumbing.php">Plumbing</a>
				    <a href="5home_hardware.php">Home Hardware</a>
				    <a href="6houseware.php">Houseware</a>
				    <a href="7lawn.php">Lawn and Outdoor</a>
				    <a href="8automotive.php">Automotive</a>
				    <a href="9appliances.php">Small Appliances</a>
				    <a href="10chemicals.php">Chemicals and Batteries</a>
				    <a href="11pets.php">Pets</a>
				  </div>
				</div>
				<div class="tab">
				  <a href="sale_and_specials.php"><button class="tablinks">Sale & Specials</button></a>
				  <button class="tablinks" onclick="location.href='../wishlist.php?customer_ID=<?php echo $_SESSION["customer_ID"]?>&w=0'">Wishlist</button>
				  <a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><button class="tablinks">Careers</button></a>
				  <a href="about_us.php"><button class="tablinks">About Us</button></a>
				</div>
				<div class="cart_tab">
					<p>Cart: 0 Items</p>
				</div>
			</div>
		</div>
		<div id="Middle"> <!--breadcrumb-->
			<div class="breadcrumbs">
				<p>Products > Paints and Sundries</p>
			</div>
		</div>
		<div id="Center"> <!--category links-->
			<div id="subcategories_container">
				<div class="subcategories_content">
					<h3>Categories</h3><br>
					<ul class="sublinks">
						<li><a href="#">All</a></li><br>
						<li><a href="#">Paints</a></li><br>
						<li><a href="#">Brushes & Rollers</a></li><br>
						<li><a href="#">Caulks & Sealants</a></li><br>
						<li><a href="#">Adhesives & Tapes</a></li><br>
						<li><a href="#">Ladders</a></li>
					</ul>
				</div>
			</div>
			<div class="grid_list">
			</div>
			<div class="pagination"> <!--PAGES-->
			  <a href="#">Previous</a>
			  <a href="#" class="active">1</a>
			  <a href="#">2</a>
			  <a href="#">3</a>
			  <a href="#">4</a>
			  <a href="#">5</a>
			  <a href="#">6</a>
			  <a href="#">Next</a>
			</div>
		</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="footer.png" height="80px" width="200px"></br>
				<a href="customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	
	</div>
</body>
</html>