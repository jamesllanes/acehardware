<?php
	session_start();
	require 'dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="css/newhome.css">
	<link rel="icon" href="ace_logo2.png" type="image/icon type">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Ace Hardware</title>
	<meta name="description" content="Write some words to describe your html page">
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="newhome.php"><img src="ace2.png" width="14%"></a>
				<div id="Login_Container1">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['customer_ID']))
							{
								echo "Welcome Guest!";?>
								<br><a href="index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['customer_ID']))
							{
								echo "Welcome<br><font color='red'><b>".$_SESSION['fullname']."</b></font>!";
								echo "<form action='logout.php' method='POST'><input type='submit' value='Logout' class='logout'/></form>";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
				<div id="Search">
					<form action="search.php" method="GET">
						<div id="Search_Content">
							<input type="text" id="search_box" name="search" placeholder="Search Product">
							<button type="submit" id="search_btn"><img src="css/images/search3.png" width="20px"></button>
						</div>
					</form>
				</div>
			</div>
				<div id="CustomerService_btn">
					<button class="button"><a href="../customer_service.php"/>CUSTOMER SERVICE</a></button>
				</div>	
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="dropdown">
				  <button class="dropbtn">Categories</button>
				  <div class="dropdown-content">
				    <a href="../1Paints_and_Sundries/paints_and_sundries.php">Paints and Sundries</a>
				    <a href="../2Tools/tools.php">Tools</a>
				    <a href="../3Electrical/electrical.php">Electrical</a>
				    <a href="../4Plumbing/plumbing.php">Plumbing</a>
				    <a href="../5Home_Hardware/home_hardware.php">Home Hardware</a>
				    <a href="../6Houseware/houseware.php">Houseware</a>
				    <a href="../7Lawn/lawn.php">Lawn and Outdoor</a>
				    <a href="../8Automotive/automotive.php">Automotive</a>
				    <a href="../9Appliances/appliances.php">Small Appliances</a>
				    <a href="../10Chemicals/chemicals.php">Chemicals and Batteries</a>
				    <a href="../11Pets/pets.php">Pets</a>
				  </div>
				</div>
				<div class="tab">
				  <a href="sale_and_specials.php"><button class="tablinks">Sale & Specials</button></a>
				  <button class="tablinks" onclick="location.href='../wishlist.php?customer_ID=<?php echo $_SESSION["customer_ID"]?>&w=0'">Wishlist</button>
				  <a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><button class="tablinks">Careers</button></a>
				  <a href="about_us.php"><button class="tablinks">About Us</button></a>
				</div>
				<div class="cart_tab">
					<?php
						if(empty($_SESSION['customer_ID']))
						{
							echo "<p><a href='cart_with_css.php'><font color='#FFFFFF'>Cart: 0 Item(s)</a></font></p>";
						}
						elseif(!empty($_SESSION['customer_ID']))
						{
							$select_cart="SELECT * FROM cart WHERE customer_ID='$_SESSION[customer_ID]'";
							$result=mysqli_query($connect,$select_cart);
							$cart_count=mysqli_num_rows($result);
						
							echo "<p><a href='cart_with_css.php'><font color='#FFFFFF'>Cart: ".$cart_count." Item(s)</a></font></p>"; 
						}
						else
						{
							echo ' ';
						}   
					?>
				</div>
			</div>
		</div>
		<!--<div id="Middle"> <!--breadcrumb--
			<div class="breadcrumbs">
				<p>Products > Paints and Sundries</p>
			</div>
		</div>-->
		<div id="Center"> <!--category links-->
			<div class="prod_content">
				<div id="about_us">
					<table>
						<tr>
							<td>
								<h1><b>Our Story</b></h1>
								<p>
									ACE Hardware, one of the leading hardware stores in the United States, opened its first franchise in the Philippines at SM Southmall, Las Piñas, Metro Manila in 1997. Since then it has had one purpose:  to bring to the Philippine market the ultimate one-stop-shop for the latest solutions for home improvement.
								</p>

								<p>
									Since its inception in 1997, ACE Hardware has grown to become one of the country’s leading home hardware chain with 199 ACE Hardware branches nationwide and has now expanded with three ACE Builders Centers and still growing.
								</p>

								<p>
									ACE Hardware’s core product ranges can be summarized under the home improvement merchandise.  Its nine main categories are:
								</p>
								<ol>
									<li>	Paints and Decorating	</li>
								   	<li>    Outdoor Living			</li>
								    <li>    Plumbing 				</li>
								    <li>    Electricals 			</li>
								    <li>    Home Organization 		</li>
								    <li>    Home Safety 			</li>
								    <li>    Home Basics 			</li>
								    <li>    Power Tools 			</li>
								    <li>    Car Care 				</li>
								</ol>

								<p>
									ACE Paints, the lead-free and odorless paint, is also available.  ACE Paints has over 2,000 colors to choose from.  With its hi-tech color matching system, one can simply bring color swatches which will be scanned by computers to get the colors in an automated and precise manner.
								</p>

								<p>
									Behind this massive project and wide array of products is a strong customer service philosophy.  ACE Hardware does not only take pride in offering quality hardware and building products, but also providing well-trained sales personnel ready to answer questions and professionally handle requirements.
								</p>

								<p>
									There are 40,000 home improvement solutions found in ACE Hardware.
								</p>

								<p>
									ACE Hardware is an affiliate of the SM Group of Companies.
								</p>
							</td>
						</tr>
					</table>
				</div>
			
				<!-- team -->
				<h2 align="center">MEET OUR TEAM</h2>
				<div class="card">
					<img class="img" src="images/teamb1.jpg" alt="James Llanes" style="width:100%;">
					<h2>James Llanes</h2>
					<p class="title">Project Manager & Web Developer</p>
					<p>De La Salle Lipa</p>
					<div style="margin: 24px 10px;">
						<a href="https://twitter.com/llanesjames07"><i style="font-size:30px;" class="fa fa-twitter"></i></a>  
						<a href="https://www.facebook.com/zeref.dragneel.deathscythe?ref=bookmarks"><i style="font-size:30px;" class="fa fa-facebook"></i></a>
						<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=deathscythe436@gmail.com"><i style="font-size:30px;" class="fa fa-envelope"></i></a> 
					</div>
				</div>

				<div class="card2">
					<img class="img" src="images/meh..jpg" alt="Ella Gutierrez" style="width:100%;">
					<h2>Ella Gutierrez</h2>
					<p class="title">Web Designer & Tester</p>
					<p>De La Salle Lipa</p>
					<div style="margin: 24px 10px;">
						<a href="https://twitter.com/login"><i style="font-size:30px;" class="fa fa-twitter"></i></a>  
						<a href="https://www.facebook.com/ella.gutierrez.92"><i style="font-size:30px;" class="fa fa-facebook"></i></a>
						<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=queen_ella_gutierrez@dlsl.edu.ph"><i style="font-size:30px;" class="fa fa-envelope"></i></a> 
					</div>
				</div>
				<!-- //team -->
			</div>
		</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="footer.png" height="80px" width="200px"></br>
				<a href="customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	</div>
</body>
</html>