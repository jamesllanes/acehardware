<?php
	session_start();
	require '../dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="css/paints.css">
	<link rel="icon" href="../ace_logo2.png" type="image/icon type">
	<title>Ace Hardware</title>
	<meta name="description" content="Write some words to describe your html page">
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="../newhome.php"><img src="css/images/ace2.png" width="14%"></a>
				<div id="Login_Container">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['customer_ID']))
							{
								echo "Welcome Guest!";?>
								<br><a href="index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['customer_ID']))
							{
								echo "Welcome<br><font color='red'><b>".$_SESSION['fullname']."</b></font>!";
								echo "<form action='../logout.php' method='POST'><input type='submit' value='Logout' class='logout'/></form>";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
				<div id="Search">
					<form action="../search.php" method="GET">
						<div id="Search_Content">
							<input type="text" id="search_box" name="search" placeholder="Search Product">
							<button type="submit" id="search_btn"><img src="css/images/search3.png" width="20px"></button>
						</div>
					</form>
				</div>
			</div>
				<div id="CustomerService_btn">
					<button class="button"><a href="../customer_service.php"/>CUSTOMER SERVICE</a></button>
				</div>
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="dropdown">
				  <button class="dropbtn">Categories</button>
				  <div class="dropdown-content">
				    <a href="../1Paints_and_Sundries/paints_and_sundries.php">Paints and Sundries</a>
				    <a href="../2Tools/tools.php">Tools</a>
				    <a href="../3Electrical/electrical.php">Electrical</a>
				    <a href="../4Plumbing/plumbing.php">Plumbing</a>
				    <a href="../5Home_Hardware/home_hardware.php">Home Hardware</a>
				    <a href="../6Houseware/houseware.php">Houseware</a>
				    <a href="../7Lawn/lawn.php">Lawn and Outdoor</a>
				    <a href="../8Automotive/automotive.php">Automotive</a>
				    <a href="../9Appliances/appliances.php">Small Appliances</a>
				    <a href="../10Chemicals/chemicals.php">Chemicals and Batteries</a>
				    <a href="../11Pets/pets.php">Pets</a>
				  </div>
				</div>
				<div class="tab">
				  <a href="../sale_and_specials.php"><button class="tablinks">Sale & Specials</button></a>
				  <button class="tablinks" onclick="location.href='../wishlist.php?customer_ID=<?php echo $_SESSION["customer_ID"]?>&w=0'">Wishlist</button>
				  <a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><button class="tablinks">Careers</button></a>
				  <a href="../about_us.php"><button class="tablinks">About Us</button></a>
				</div>
				<div class="cart_tab">
					<?php
						if(empty($_SESSION['customer_ID']))
						{
							echo "<p><a href='../cart_with_css.php'><font color='#FFFFFF'>Cart: 0 Item(s)</a></font></p>";
						}
						elseif(!empty($_SESSION['customer_ID']))
						{
							$select_cart="SELECT * FROM cart WHERE customer_ID='$_SESSION[customer_ID]'";
							$result=mysqli_query($connect,$select_cart);
							$cart_count=mysqli_num_rows($result);
						
							echo "<p><a href='../cart_with_css.php'><font color='#FFFFFF'>Cart: ".$cart_count." Item(s)</a></font></p>"; 
						}
						else
						{
							echo ' ';
						}   
					?>
				</div>
			</div>
		</div>
		<div id="Middle"> <!--breadcrumb-->
			<div class="breadcrumbs">
				<p>Products > Houseware > Bathroom Accessories</p>
			</div>
		</div>
		<div id="Center"> <!--category links-->
			<div id="subcategories_container">
				<div class="subcategories_content">
					<h3>Categories</h3><br>
					<ul class="sublinks">
						<li><a href="houseware.php">All</a></li><br>
						<li><a href="1bathroom.php" class="active">Bathroom Accessories</a></li><br>
						<li><a href="2cleaning.php">Cleaning Implements</a></li><br>
						<li><a href="3dish_drainers.php">Dish Drainers & Kitchen</a></li><br>
						<li><a href="5drawers.php">Drawers & Closets</a></li><br>
						<li><a href="6laundry_implements.php">Laundry Implements</a></li><br>
						<li><a href="7mats.php">Mats & Carpets</a></li><br>
						<li><a href="8pails.php">Pails & Buckets</a></li><br>
						<li><a href="9stackables.php">Stackables & Storage Accessories</a></li><br>
						<li><a href="10trash_bins.php">Trash Bins</a></li><br>					
					</ul>
				</div>
			</div>
			<div class="prod_content">
				<?php
					$ID=$_SESSION['customer_ID'];
					$query="SELECT * FROM inventory WHERE Category LIKE '%Houseware%' AND Sub_Category LIKE '%Bathroom Accessories%'";
					$result=mysqli_query($connect,$query);

					if(mysqli_num_rows($result)>0)
					{?>
						<form action="houseware.php?Prod_ID=<?php echo $_SESSION['Prod_ID']?>&Price=<?php echo $_SESSION['Price']?>"; method='POST'/><?php
						echo "<table frame=box bordercolor='#8C001A' class='prod_list' style='width:750px; height: 358px;'>";
						echo "<tr><th colspan='4' class='title'>List of Products<br></th></tr>";
						echo "<tr class='borderbottom1'><th>Product ID</th>";
						echo "<th style='width:65%'>Product Name</th>";
						echo "<th style='width:15%'>Price</th>";
						echo "<th>Select</th>";
						//echo "<th style='width:40px' colspan=2>Quantity</th>";
						while($row=mysqli_fetch_array($result))
						{?>
							<?php

							$Prod_ID=$row['Prod_ID'];
							$_SESSION['Prod_ID']=$Prod_ID;
							$Prod_ID=$_SESSION['Prod_ID'];

							$Prod_Name=$row['Prod_Name'];
							$_SESSION['Prod_Name']=$Prod_Name;
							$Prod_Name=$_SESSION['Prod_Name'];

							$Price=$row['Price'];
							$_SESSION['Price']=$Price;
							$Price=$_SESSION['Price'];

							echo "<tr class='borderbottom'>";
							echo "<td>".$Prod_ID."</td>";
							echo "<td><a href='houseware_product_page2.php?Prod_ID=$Prod_ID&Quantity=1&customer_ID=$ID'>".$Prod_Name."</a></td>";
							echo "<td>₱".$Price."</td>";
							echo "<td><a href='houseware_product_page2.php?Prod_ID=$Prod_ID&Quantity=1&customer_ID=$ID'><img src='css/images/search3.png' width='20px';/></a></td>";


							echo "</tr>"; 	
						}
						echo "</table>";
						echo "</form>";
					}	
				?>
			</div>
		</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="../footer.png" height="80px" width="200px"></br>
				<a href="../customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="../terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="../privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="../1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="../2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="../3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="../4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="../5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="../6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="../7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="../8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="../9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="../10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="../11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="../cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="../fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="../logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	</div>
</body>
</html>