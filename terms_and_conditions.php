<?php
	session_start();
	require 'dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="css/cart.css">
	<link rel="icon" href="ace_logo2.png" type="image/icon type">
	<title>Ace Hardware</title>
	<meta name="description" content="Write some words to describe your html page">
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="newhome.php"><img src="ace2.png" width="14%"></a>
				<div id="Login_Container1">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['customer_ID']))
							{
								echo "Welcome Guest!";?>
								<br><a href="../index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['customer_ID']))
							{
								echo "Welcome<br><font color='red'><b>".$_SESSION['fullname']."</b></font>!";
								echo "<form action='../logout.php' method='POST'><input type='submit' value='Logout' class='logout'/></form>";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
				<div id="Search">
					<form action="search.php" method="GET">
						<div id="Search_Content">
							<input type="text" id="search_box" name="search" placeholder="Search Product">
							<button type="submit" id="search_btn"><img src="css/images/search3.png" width="20px"></button>
						</div>
					</form>
				</div>
			</div>
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="dropdown">
				  <button class="dropbtn">Categories</button>
				  <div class="dropdown-content">
				    <a href="../1Paints_and_Sundries/paints_and_sundries.php">Paints and Sundries</a>
				    <a href="../2Tools/tools.php">Tools</a>
				    <a href="../3Electrical/electrical.php">Electrical</a>
				    <a href="../4Plumbing/plumbing.php">Plumbing</a>
				    <a href="../5Home_Hardware/home_hardware.php">Home Hardware</a>
				    <a href="../6Houseware/houseware.php">Houseware</a>
				    <a href="../7Lawn/lawn.php">Lawn and Outdoor</a>
				    <a href="../8Automotive/automotive.php">Automotive</a>
				    <a href="../9Appliances/appliances.php">Small Appliances</a>
				    <a href="../10Chemicals/chemicals.php">Chemicals and Batteries</a>
				    <a href="../11Pets/pets.php">Pets</a>
				  </div>
				</div>
				<div class="tab">
				  <a href="sale_and_specials.php"><button class="tablinks">Sale & Specials</button></a>
				  <button class="tablinks" onclick="location.href='../wishlist.php?customer_ID=<?php echo $_SESSION["customer_ID"]?>&w=0'">Wishlist</button>
				  <a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><button class="tablinks">Careers</button></a>
				  <a href="about_us.php"><button class="tablinks">About Us</button></a>
				</div>
				<div class="cart_tab">
					<?php
						if(empty($_SESSION['customer_ID']))
						{
							echo "<p><a href='cart_with_css.php'><font color='#FFFFFF'>Cart: 0 Item(s)</a></font></p>";
						}
						elseif(!empty($_SESSION['customer_ID']))
						{
							$select_cart="SELECT * FROM cart WHERE customer_ID='$_SESSION[customer_ID]'";
							$result=mysqli_query($connect,$select_cart);
							$cart_count=mysqli_num_rows($result);
						
							echo "<p><a href='cart_with_css.php'><font color='#FFFFFF'>Cart: ".$cart_count." Item(s)</a></font></p>"; 
						}
						else
						{
							echo ' ';
						}   
					?>
				</div>
			</div>
		</div>
		<div id="Center"> <!--category links-->
			<div class="prod_content">
				<div class="terms-and-conditons">
					<table>
						<tr>
							<td>
								<h1>Terms and Conditions</h1>
								<p>
									The following Terms of Use shall govern your use and access of the Platform and of the services therein. The term “Platform” pertains to the web and mobile versions of the website operated and/or owned by Ace Hardware Philippines, Inc. ("Ace Hardware" or "We" or "Us") at ______________ and the mobile applications made available by it.
								</p>
								<p>
									By accessing the Platform and/or using any of the services therein, you represent that you are at least 18 years old and agree, without limitation or qualification, to be bound by these Terms of Use.
								</p>
								<ul>            
									<li>
										Additional Terms and Conditions. Ace Hardware’s Privacy Policy, which can be found here, applies to your use of the Platform and of the services therein. Additional policies, guidelines, and terms and conditions may apply to certain content, features, products, and services available on the Platform, including that for the Ace Rewards Program. The terms of Ace Hardware’s Privacy Policy and such other policies, guidelines, and terms and conditions are made integral part of these Terms of Use.
									</li>
									<li>
										Products, Pricing, Content and Specifications. The products and services featured on the Platform (collectively, the "Products") and its contents, specifications, and prices are subject to change at any time, without need of prior notice. The prices of the Products may be different from that sold in Ace Hardware physical stores.
									</li>
									<li>
										Payment – You may pay using any of the payment methods prescribed on the Platform. When you place an order, actual payment will only be charged upon the acceptance of your order. All payments shall be made to Ace Hardware, either accepting payments in its own right or through a third party service provider engaged by Ace Hardware.
									</li>
									<li>
										Invoicing – The Ace Hardware Online Store may invoice you upon the due date of any payment under a Customer Contract.The Platform will provide you the Sales Invoice upon delivery of your item/s or upon pick-up at the designated pick-up branch.
									</li>
									<li>
										Shipping, Delivery and Store Pickup Limitations. When an order is placed on the Platform, you have the following options:
									</li>
								</ul>
								<p><b>Delivery</b></p>
								<p>
									Product(s) will be delivered to an address designated by you, as long as that shipping address is compliant with our service provider’s shipping restrictions, within 3-5 business days from confirmation of order.
									Delivery Fees
									Applicable delivery fees will be charged
								</p>
								<p><b>Store Pickup</b></p>
								<p>
									Product(s) may be picked up at any of the following branches ofAce Hardware Philippines Inc.:
								</p>
								<ul>
									<li>Ace Hardware SM Megamall</li>
									<li>Ace Hardware North Edsa</li>
									<li>Ace Hardware Market! Market!</li>
									<li>Ace Hardware SM Makati</li>
									<li>ACE Hardware SM Southmall</li>
									<li>ACE Hardware SM City Bacolod</li>
									<li>ACE Hardware SM City Bacoor</li>
									<li>ACE Hardware SM City Cebu</li>
									<li>ACE Hardware SM City Iloilo</li>
									<li>ACE Hardware SM City Davao</li>
									<li>ACE Hardware SM City Baguio</li>
									<li>ACE Hardware SM City Pampanga</li>
									<li>ACE Builders North EDSA</li>
									<li>ACE Hardware SM City Calamba</li>
								</ul>
								<p>
									Products purchased for store pickup are subject to Ace Hardware’s store pickup limitations and restrictions. Products purchased for store pickup are subject to Ace Hardware’s store pickup limitations and restrictions.
								</p>
								<ul>
									<li>The customer will receive a One Time Pin (OTP) when the item is ready for pick-up.</li>
									<li>Customer will present the OTP to the Ace Personnel.</li>
									<li>In case a customer deleted the OTP, Ace can resend the OTP at point of pick-up.</li>
									<li>An authorized representative may pick-up the item by presenting the following:</li>
									<li>Authorization letter from the owner</li>
									<li>Photocopy of the owner’s valid ID</li>
									<li>Photocopy of the authorized representative’s ID</li>
									<li>Copy of the OTP</li>
								</ul>
								<p>
									Failure to Pay - If a Customer fails to make any payment pursuant to the payment method selected or payment is cancelled for any reason whatsoever, Ace Hardware shall cancel the order and suspend delivery of the Products until payment is made in full.  
								</p>
								<ul>
									<li>
										Return Policy- All returns/exchanges must be done in-store at any Ace Hardware branch in accordance with Ace Hardware’s Return and Exchange Policy.
									</li>
									<li>
										Accuracy of Information. Ace Hardware endeavors to provide an accurate description of the Products, but we do not warrant that such description, color, information or other content available on the Site are accurate, complete, reliable, current or free from error. The weight, measurements and other descriptions provided on the Platform are approximate figures and are provided for convenience purposes only. We make all reasonable efforts to accurately display the attributes of the Products, including the applicable colors. However, the color you see on the Platform may differ from the actual color, depending on your mobile device, computer system, monitor, and/or other display features.

										The Platform may contain typographical errors or inaccuracies and may not be complete or current. Ace Hardware therefore reserves the right to correct any errors, inaccuracies or omissions (including after an order has been submitted) and to change or update information at any time without prior notice. Please note that such errors, inaccuracies or omissions may relate to pricing and availability, and we reserve the right to cancel or refuse to accept any order placed based on incorrect pricing or availability information.
									</li>
									<li>
										Acceptance of Order
										While it is our practice to confirm orders by email, the receipt of an email order confirmation does not constitute our acceptance of an order or our confirmation of an offer to sell a Product. We reserve the right, without prior notice, to limit the order quantity on any Product, to cancel any order at any time, and/or to refuse service to any customer. We also may require verification of information prior to the acceptance and/or shipment of any order.
									</li>
									<li>
										Use of the Platform
										In using the Platform and the services therein, you agree to:
										<ol type="a">
											<li>
												do soonly for lawful purposes;
											</li>
											<li>
												ensure that all information or data you provide on the Platform are accurate and agree to take sole responsibility for such information and data;
											</li>
											<li>
												be responsible for maintaining the confidentiality of your account information and password and for restricting access to such information and to your computer. You agree to accept responsibility for all activities that occur under your account, whether such activity is authorized or not. You should notify us immediately if you have knowledge that or have reason for suspecting that the confidentiality of your account has been compromised or if there has been any unauthorized use thereof
											</li>
										</ol>
									</li>
									<li>
										Intellectual Property

										All information and content available on the Platform, including but not limited to trademarks, logos, service marks, features, functions, text, graphics, logos, button icons, images, audio clips, data compilations and software, and the compilation and organization thereof (collectively, the "Content") are our property or the property of our parents, subsidiaries, affiliates, partners or licensors, and are protected by international laws, including laws governing copyrights and trademarks.

										Except as required under applicable law, neither the Content nor any portion of the Platform may be used, reproduced, duplicated, copied, sold, resold, accessed, modified, or otherwise exploited, in whole or in part, for any purpose without our express, prior written consent. Nothing contained on the Platform grants or should be construed as granting, by implication, estoppel, or otherwise, any license or right to use any trademarks, trade names, service marks or logos displayed on the Platform, without the written permission of Ace Hardware or such third party owner, as may be applicable.
									</li>
									<li>
										Third Party Links. From time to time, the Platform may contain links to websites, applications, or online or digital sites or services that are not owned, operated or controlled by Ace Hardware or our affiliates, subsidiaries or partners. All such links are provided solely as a convenience to you and are not an endorsement by us, our parents, subsidiaries, our affiliates or our partners. If you use these links, you will leave the Platform and your use of the said links will be at your own risk. Neither Ace Hardware, nor any of our affiliates, subsidiaries or partners are responsible for any content, materials or other information located on or accessible from any other website, application, or online or digital site or service. We are in no way responsible for examining or evaluating, and we do not warrant the offerings of, off-website pages or any other websites linked to or from the Platform, nor do we assume any responsibility or liability for the actions, content, products, or services of such pages and websites, including, without limitation, their privacy policies and terms and conditions.
									</li>
									<li>
										User Information. Other than Personal Data as defined under Republic Act No. 10173 (Data Privacy Act of 2012), which is subject to Ace Hardware’s Privacy Policy, any material, information, suggestions, idea, concept, know-how, technique, question, comment or other communication you transmit, upload, or post to or through the Platform in any manner ("User Communications") are and will be considered non-confidential and non-proprietary. Ace Hardware may use any or all User Communications for any purpose whatsoever, including, without limitation, reproduction, transmission, disclosure, publication, broadcast, development, deletion and manufacturing and/or marketing in any manner whatsoever for any or all commercial or non-commercial purposes. We may, but are not obligated to, monitor or review any User Communications. We shall have no obligation to use, return, review, or respond to any User Communications. We will have no liability related to the content of any such User Communications, whether or not arising under the laws of copyright, libel, privacy, obscenity, or otherwise. However, we retain the right to remove any or all User Communications that includes any material we deem inappropriate or unacceptable.
									</li>
									<li>
										LIMITATIONS OF LIABILITY.
										YOU AGREE THAT NEITHER WE NOR OUR PARENTS, SUBSIDIARIES, AFFILIATES OR PARTNERS, OR OUR/THEIR RESPECTIVE DIRECTORS, OFFICERS, SHAREHOLDERS, EMPLOYEES AND AGENTS WILL BE RESPONSIBLE OR LIABLE FOR ANY (a) BUSINESS INTERRUPTION; (b) DELAY/INTERRUPTION IN ACCESS TO THE PLATFORM; (c) DATA NON-DELIVERY, MISDELIVERY, CORRUPTION, DESTRUCTION OR OTHER MODIFICATION; (d) LOSS OR DAMAGES OF ANY SORT INCURRED AS A RESULT OF ACCESSING THE THIRD PARTY LINKS ON THE PLATFORM; (e) COMPUTER VIRUSES, SYSTEM FAILURES OR MALFUNCTIONS IN CONNECTION WITH USE OF THE PLATFORM; OR (f) ANY INACCURACIES OR OMISSIONS IN THE CONTENT OF THE PLATFORM

										YOU AGREE THAT YOUR SOLE REMEDY, IF ANY, IS FROM THE MANUFACTURER OR SUPPLIER OF THE PRODUCTS, IN ACCORDANCE WITH SUCH MANUFACTURER'S OR SUPPLIER'S WARRANTY, OR TO SEEK A RETURN OR REFUND FOR SUCH PRODUCTS IN ACCORDANCE WITH OUR RETURN AND EXCHANGE POLICY
									</li>
									<li>
										Revisions to these Terms and Conditions. We may revise these Terms of Use at any time and from time to time by updating this posting. You should visit this page from time to time to review the then current Terms of Use because they are binding on you.
									</li>
									<li>
										Termination. You or we may suspend or terminate your account or your use of the Platform at any time, for any reason or for no reason. You are personally liable for any orders tor charges that you incur prior to termination. We reserve the right to change, suspend, or discontinue all or any aspect of the Platform at any time without notice.
									</li>
									<li>
										Additional Assistance. If you have any questions or comments, we invite you to email us at
									</li>
								</ul>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="footer.png" height="80px" width="200px"></br>
				<a href="customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	</div>
</body>
</html>