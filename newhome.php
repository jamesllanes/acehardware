<?php
	session_start();
	require 'dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="css/newhome.css">
	<link rel="icon" href="ace_logo2.png" type="image/icon type">
	<title>Ace Hardware</title>
	<meta name="description" content="Ace Hardware is one of the leading hardware stores in the United States. The company’s mission is to be a retail support company, providing its dealer to owner merchandise and services at the lowest possible end cost. It opened its first franchise in the Philippines at SM Southmall, Las Piñas, Metro Manila in 1997 and has grown to be one of our country’s home hardware chains and is still growing.">
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="newhome.php"><img src="ace2.png" width="14%"></a>
				<div id="Login_Container1">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['customer_ID']))
							{
								echo "Welcome Guest!";?>
								<br><a href="index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['customer_ID']))
							{
								echo "Welcome<br><font color='red'><b>".$_SESSION['fullname']."</b></font>!";
								echo "<form action='logout.php' method='POST'><input type='submit' value='Logout' class='logout'/></form>";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
				<div id="Search">
					<form action="search.php" method="GET">
						<div id="Search_Content">
							<input type="text" id="search_box" name="search" placeholder="Search Product"/>
							<button type="submit" id="search_btn" onclick="location='search.php'"><img src="search3.png" width="20px"></button>
						</div>
					</form>
				</div>
			</div>
				<div id="CustomerService_btn">
					<button class="button"><a href="customer_service.php"/>CUSTOMER SERVICE</a></button>
				</div>
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="dropdown">
				  <button class="dropbtn">Categories</button>
				  <div class="dropdown-content">
				    <a href="1Paints_and_Sundries/paints_and_sundries.php">Paints and Sundries</a>
				    <a href="2Tools/tools.php">Tools</a>
				    <a href="3Electrical/electrical.php">Electrical</a>
				    <a href="4Plumbing/plumbing.php">Plumbing</a>
				    <a href="5Home_Hardware/home_hardware.php">Home Hardware</a>
				    <a href="6Houseware/houseware.php">Houseware</a>
				    <a href="7Lawn/lawn.php">Lawn and Outdoor</a>
				    <a href="8Automotive/automotive.php">Automotive</a>
				    <a href="9Appliances/appliances.php">Small Appliances</a>
				    <a href="10Chemicals/chemicals.php">Chemicals and Batteries</a>
				    <a href="11Pets/pets.php">Pets</a>
				  </div>
				</div>
				<div class="tab">
				  <a href="sale_and_specials.php"><button class="tablinks">Sale & Specials</button></a>
				  <button class="tablinks" onclick="location.href='../wishlist.php?customer_ID=<?php echo $_SESSION["customer_ID"]?>&w=0'">Wishlist</button>
				  <a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><button class="tablinks">Careers</button></a>
				  <a href="about_us.php"><button class="tablinks">About Us</button></a>
				</div>
				<div class="cart_tab">
					<?php
						if(empty($_SESSION['customer_ID']))
						{
							echo "<p><a href='cart_with_css.php'><font color='#FFFFFF'>Cart: 0 Item(s)</a></font></p>";
						}
						elseif(!empty($_SESSION['customer_ID']))
						{
							$select_cart="SELECT * FROM cart WHERE customer_ID='$_SESSION[customer_ID]'";
							$result=mysqli_query($connect,$select_cart);
							$cart_count=mysqli_num_rows($result);
						
							echo "<p><a href='cart_with_css.php'><font color='#FFFFFF'>Cart: ".$cart_count." Item(s)</a></font></p>"; 
						}
						else
						{
							echo ' ';
						}   
					?>
				</div>
			</div>
		</div>
		<div id="Middle"> <!--slideshow-->
			<div class="slideshow-container">
			<div class="mySlides fade">
			  <!--<div class="numbertext">1 / 3</div><a href="#">-->
			  <img src="slideshow1.png">
			  <!--<div class="text">Construction</div>-->
			</div>

			<div class="mySlides fade">
			  <!--<div class="numbertext">2 / 3</div><a href="#">-->
			  <img src="slideshow2.png">
			  <!--<div class="text">Home Appliances</div>-->
			</div>

			<div class="mySlides fade">
			  <!--<div class="numbertext">3 / 3</div><a href="#">-->
			  <img src="slideshow3.png">
			  <!--<div class="text">Car Tools And Accessories</div>-->
			</div>

			</div>
			<br>

			<div class="dot-div">
			  <span class="dot" style="position:relative;left:460px;top:270px;"></span> 
			  <span class="dot" style="position:relative;left:460px;top:270px;"></span> 
			  <span class="dot" style="position:relative;left:460px;top:270px;"></span> 
			</div>
		
			<script>
			var slideIndex = 0;
			showSlides();

			function showSlides() {
			    var i;
			    var slides = document.getElementsByClassName("mySlides");
			    var dots = document.getElementsByClassName("dot");
			    for (i = 0; i < slides.length; i++) {
			       slides[i].style.display = "none";  
			    }
			    slideIndex++;
			    if (slideIndex > slides.length) {slideIndex = 1}    
			    for (i = 0; i < dots.length; i++) {
			        dots[i].className = dots[i].className.replace(" active", "");
			    }
			    slides[slideIndex-1].style.display = "block";  
			    dots[slideIndex-1].className += " active";
			    setTimeout(showSlides, 3000); 
			}
			</script>
		</div>
		<div id="Center"> <!--category links-->
			<div class="wrapper">
				<div class="one">
					<a class="categorynames">Paints and Sundries</a><br>
					<a href="1Paints_and_Sundries/1paint.php" class="categorylinks">Paints</a><br>
					<a href="1Paints_and_Sundries/2brushes.php" class="categorylinks">Brushes & Rollers</a><br>
					<a href="1Paints_and_Sundries/3caulks.php" class="categorylinks">Caulks & Sealants</a><br>
					<a href="1Paints_and_Sundries/4adhesives.php" class="categorylinks">Adhesives & Tapes</a><br>
					<a href="1Paints_and_Sundries/5ladders.php" class="categorylinks">Ladders</a><br>
					<a href="1Paints_and_Sundries/paints_and_sundries.php" class="viewall">VIEW ALL ></a>
				</div>
				<div class="two">
					<a class="categorynames">Tools</a><br>
					<a href="2Tools/1power_tools.php" class="categorylinks">Power Tools</a><br>
					<a href="2Tools/2hand_tools.php" class="categorylinks">Hand Tools</a><br>
					<a href="2Tools/3measuring_tools.php" class="categorylinks">Measuring Tools</a><br>
					<a href="2Tools/4tool_organizers.php" class="categorylinks">Tool Organizers</a><br>
					<a href="2Tools/tools.php" class="viewall">VIEW ALL ></a>
				</div>
				<div class="three">
					<a class="categorynames">Electrical</a><br>
					<a href="3Electrical/1bulbs.php" class="categorylinks">Bulbs & Flourecent Lights</a><br>
					<a href="3Electrical/2lightning.php" class="categorylinks">Lighting Fixtures</a><br>
					<a href="3Electrical/3flashlight.php" class="categorylinks">Flashlight & Batteries</a><br>
					<a href="3Electrical/4rechargeables.php" class="categorylinks">Rechargeables</a><br>
					<a href="3Electrical/5power_supply.php" class="categorylinks">Power Supply</a><br>
					<a href="3Electrical/6extension_cords.php" class="categorylinks">Extension Cords, Wires & Cables</a><br>
					<a href="3Electrical/7wiring_devices.php" class="categorylinks">Wiring Devices</a><br>
					<a href="3Electrical/8audio.php" class="categorylinks">Audio, Video & Telephone</a><br>
					<a href="3Electrical/9supplies.php" class="categorylinks">Supplies</a><br>
					<a href="3Electrical/electrical.php" class="viewall">VIEW ALL ></a>
				</div>
				<div class="four">
					<a class="categorynames">Plumbing</a><br>
					<a href="4Plumbing/2faucets.php" class="categorylinks">Faucets</a><br>
					<a href="4Plumbing/4showers.php" class="categorylinks">Showers & Bidets</a><br>
					<a href="4Plumbing/5water_filtration.php" class="categorylinks">Water Filtration</a><br>
					<a href="4Plumbing/6water_heaters.php" class="categorylinks">Water Heaters</a><br>
					<a href="4Plumbing/7water_storage.php" class="categorylinks">Water Storage & Pumps</a><br>
					<a href="4Plumbing/8water_closet.php" class="categorylinks">Water Closet & Accessories</a><br>
					<a href="4Plumbing/9sink.php" class="categorylinks">Sink, Lavatory & Accessories</a><br>
					<a href="4Plumbing/plumbing.php" class="viewall">VIEW ALL ></a>
				</div>
				<div class="five">
					<a class="categorynames">Home Hardware</a><br>
			  		<a href="5Home_Hardware/1building_materials.php" class="categorylinks">Building Materials & Supplies</a><br>
					<a href="5Home_Hardware/2doors.php" class="categorylinks">Door & Cabinet Hardware</a><br>
					<a href="5Home_Hardware/3home_safety.php" class="categorylinks">Home Safety & Security</a><br>
					<a href="5Home_Hardware/4industrial.php" class="categorylinks">Industrial & Exhaust Fans</a><br>
					<a href="5Home_Hardware/5locksets.php" class="categorylinks">Locksets & Padlocks</a><br>
					<a href="5Home_Hardware/6utility_racks.php" class="categorylinks">Utility Racks & Carts</a><br>
					<a href="5Home_Hardware/7wall_mounts.php" class="categorylinks">Wall Mounts & Shelvings</a><br>
					<a href="5Home_Hardware/home_hardware.php" class="viewall">VIEW ALL ></a>
				</div>
				<div class="six">
					<a class="categorynames">Houseware</a><br>
					<a href="6Houseware/1bathroom.php" class="categorylinks">Bathroom Accessories</a><br>
					<a href="6Houseware/2cleaning.php" class="categorylinks">Cleaning Implements</a><br>
					<a href="6Houseware/3dish_drainers.php" class="categorylinks">Dish Drainers & Kitchen</a><br>
					<a href="6Houseware/5drawers.php" class="categorylinks">Drawers & Closets</a><br>
					<a href="6Houseware/6laundry_implements.php" class="categorylinks">Laundry Implements</a><br>
					<a href="6Houseware/7mats.php" class="categorylinks">Mats & Carpets</a><br>
					<a href="6Houseware/9stackables.php" class="categorylinks">Stackables & Storage Accessories</a><br>
					<a href="6Houseware/10trash_bins.php" class="categorylinks">Trash Bins</a><br>
					<a href="6Houseware/houseware.php" class="viewall">VIEW ALL ></a>
				</div>
				<div class="seven">
					<a class="categorynames">Lawn and Outdoor</a><br>
					<a href="7Lawn/1coolers.php" class="categorylinks">Coolers & Jugs</a><br>
					<a href="7Lawn/2gardening.php" class="categorylinks">Gardening Tools</a><br>
					<a href="7Lawn/3grills.php" class="categorylinks">Grills & Outdoor Cooking</a><br>
					<a href="7Lawn/4hoses.php" class="categorylinks">Hoses,Sprayers & Sprinklers</a><br>
					<a href="7Lawn/5mowers.php" class="categorylinks">Mowers & Trimmers</a><br>
					<a href="7Lawn/8tents.php" class="categorylinks">Tents & Camping</a><br>
					<a href="7Lawn/lawn.php" class="viewall">VIEW ALL ></a>
				</div>
				<div class="eight">
					<a class="categorynames">Automotive</a><br>
					<a href="8Automotive/1accessories.php" class="categorylinks">Accessories</a><br>
					<a href="8Automotive/2car_audio.php" class="categorylinks">Car Audio</a><br>
					<a href="8Automotive/3cleaning_supplies.php" class="categorylinks">Cleaning Supplies</a><br>
					<a href="8Automotive/4lightning.php" class="categorylinks">Lightning</a><br>
					<a href="8Automotive/5maintenance.php" class="categorylinks">Maintenance</a><br>
					<a href="8Automotive/6tools.php" class="categorylinks">Tools</a><br>
					<a href="8Automotive/automotive.php" class="viewall">VIEW ALL ></a>
				</div>
				<div class="nine">
					<a class="categorynames">Small Appliances</a><br>
					<a href="9Appliances/1air_coolers.php" class="categorylinks">Air Coolers</a><br>
					<a href="9Appliances/2aircon.php" class="categorylinks">Aircon</a><br>
					<a href="9Appliances/3beds.php" class="categorylinks">Beds & Furniture</a><br>
					<a href="9Appliances/4fans.php" class="categorylinks">Fans</a><br>
					<a href="9Appliances/5household_appliances.php" class="categorylinks">Household Appliances</a></br>
					<a href="9Appliances/6stools.php" class="categorylinks">Stools, Chairs & Tables</a></br>
					<a href="9Appliances/appliances.php" class="viewall">VIEW ALL ></a>
				</div>
				<div class="ten">
			        <a class="categorynames">Chemicals and Batteries</a><br>
					<a href="10Chemicals/1batteries.php" class="categorylinks">Batteries</a><br>
					<a href="10Chemicals/2household.php" class="categorylinks">Household Chemicals</a><br>
					<a href="10Chemicals/3insect.php" class="categorylinks">Insect & Pest Control</a><br>
					<a href="10Chemicals/chemicals.php" class="viewall">VIEW ALL ></a>
				</div>
				<div class="eleven">
					<a class="categorynames">Pets</a><br>
					<a href="11Pets/1pet_accessories.php" class="categorylinks">Pet Accessories</a><br>
					<a href="11Pets/2pet_care.php" class="categorylinks">Pet Care & Grooming</a><br>
					<a href="11Pets/3pet_food.php" class="categorylinks">Pet Food</a><br>
					<a href="11Pets/pets.php" class="viewall">VIEW ALL ></a>
				</div>
			</div>
		</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="footer.png" height="80px" width="200px"></br>
				<a href="customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	</div>
</body>
</html>