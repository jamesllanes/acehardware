<?php
	session_start();
	require '../dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="css/paints.css">
	<link rel="icon" href="../ace_logo2.png" type="image/icon type">
	<title>Ace Hardware</title>
	<meta name="description" content="Write some words to describe your html page">
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="../newhome.php"><img src="css/images/ace2.png" width="14%"></a>
				<div id="Login_Container">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['customer_ID']))
							{
								echo "Welcome Guest!";?>
								<br><a href="../index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['customer_ID']))
							{
								echo "Welcome<br><font color='red'><b>".$_SESSION['fullname']."</b></font>!";
								echo "<form action='../logout.php' method='POST'><input type='submit' value='Logout' class='logout'/></form>";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
				<div id="Search">
					<form action="../search.php" method="GET">
						<div id="Search_Content">
							<input type="text" id="search_box" name="search" placeholder="Search Product">
							<button type="submit" id="search_btn"><img src="css/images/search3.png" width="20px"></button>
						</div>
					</form>
				</div>
			</div>
				<div id="CustomerService_btn">
					<button class="button"><a href="../customer_service.php"/>CUSTOMER SERVICE</a></button>
				</div>
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="dropdown">
				  <button class="dropbtn">Categories</button>
				  <div class="dropdown-content">
				    <a href="../1Paints_and_Sundries/paints_and_sundries.php">Paints and Sundries</a>
				    <a href="../2Tools/tools.php">Tools</a>
				    <a href="../3Electrical/electrical.php">Electrical</a>
				    <a href="../4Plumbing/plumbing.php">Plumbing</a>
				    <a href="../5Home_Hardware/home_hardware.php">Home Hardware</a>
				    <a href="../6Houseware/houseware.php">Houseware</a>
				    <a href="../7Lawn/lawn.php">Lawn and Outdoor</a>
				    <a href="../8Automotive/automotive.php">Automotive</a>
				    <a href="../9Appliances/appliances.php">Small Appliances</a>
				    <a href="../10Chemicals/chemicals.php">Chemicals and Batteries</a>
				    <a href="../11Pets/pets.php">Pets</a>
				  </div>
				</div>
				<div class="tab">
				  <a href="../sale_and_specials.php"><button class="tablinks">Sale & Specials</button></a>
				   <button class="tablinks" onclick="location.href='../wishlist.php?customer_ID=<?php echo $_SESSION["customer_ID"]?>&w=0'">Wishlist</button>
				  <a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><button class="tablinks">Careers</button></a>
				  <a href="../about_us.php"><button class="tablinks">About Us</button></a>
				</div>
				<div class="cart_tab">
					<?php
						if(empty($_SESSION['customer_ID']))
						{
							echo "<p><a href='../cart_with_css.php'><font color='#FFFFFF'>Cart: 0 Item(s)</a></font></p>";
						}
						elseif(!empty($_SESSION['customer_ID']))
						{
							$select_cart="SELECT * FROM cart WHERE customer_ID='$_SESSION[customer_ID]'";
							$result=mysqli_query($connect,$select_cart);
							$cart_count=mysqli_num_rows($result);
						
							echo "<p><a href='../cart_with_css.php'><font color='#FFFFFF'>Cart: ".$cart_count." Item(s)</a></font></p>"; 
						}
						else
						{
							echo ' ';
						}   
					?>
				</div>
			</div>
		</div>
		<div id="Middle"> <!--breadcrumb-->
			<div class="breadcrumbs">
				<p>Products > Pets</p>
			</div>
		</div>
		<div id="Center"> <!--category links-->
			<div id="subcategories_container">
				<div class="subcategories_content">
					<h3>Categories</h3><br>
					<ul class="sublinks">
						<li><a href="pets.php">All</a></li><br>
						<li><a href="1pet_accessories.php">Pet Accessories</a></li><br>
						<li><a href="2pet_care.php">Pet Care & Grooming</a></li><br>
						<li><a href="3pet_food.php">Pet Food</a></li><br>
					</ul>
				</div>
			</div>
			<div class="prod_content">
				<?php
					//$_SESSION['Pets'];

					$query="SELECT * FROM inventory WHERE Prod_ID LIKE '%$_REQUEST[Prod_ID]%'";
					$result=mysqli_query($connect,$query);

					if(mysqli_num_rows($result)>0)
					{?>
						<form action="pets_product_page2.php?Prod_ID=<?php echo $_SESSION['Prod_ID']?>&Price=<?php echo $_SESSION['Price']?>"; method='POST'/><?php
						echo "<table frame=box bordercolor='#8C001A' class='prod_list' style='width:750px; height: 358px;'>";
						//echo "<th style='width:40px' colspan=2>Quantity</th>";
						while($row=mysqli_fetch_array($result))
						{
							if($row['Prod_ID']==$_REQUEST['Prod_ID'])
							{
								$Prod_ID=$row['Prod_ID'];
								$_SESSION['Prod_ID']=$Prod_ID;
								$Prod_ID=$_SESSION['Prod_ID'];

								$_SESSION['Prod_Name']=$row['Prod_Name'];
								$Prod_Name=$_SESSION['Prod_Name'];

								$Price=$row['Price'];
								$_SESSION['Price']=$Price;
								$Price=$_SESSION['Price'];

								$customer_ID=$_REQUEST['customer_ID'];
								$qty=$_REQUEST['Quantity'];

								$image=$row['Image'];
								$_SESSION['Image']=$image;
								$img=$_SESSION['Image'];
								
								echo "<tr><th colspan='3' class='title'>".$row['Prod_Name']."<br></th></tr>";
								echo "<tr><td rowspan='5' style='width: 380px;'><img height='250px' width='300px' src='images/".$img."'/></td>";
								echo "<td><b>Product ID:</b></td>";
								echo "<td>".$Prod_ID."</td></tr>";
								echo "<td colspan='2'><b>Description:</b></br></br>".$row['Description']."</td></tr>";
								echo "<tr><td><b>Price:</b></td><td>₱".$Price."</td>";
								echo "</tr><td><b>Quantity:</b></td>";

								/*FOR QUANTITY AND ADD TO CART LINK*/
								if ($qty==0)
								{
									$_SESSION['Quantity']=null;
								}
								else
								{
									$_SESSION['Quantity']=$qty;
									?><?php
								}
								echo "<td><input type='text' readonly name='quantity' style='width:60%' value='".$qty."'/>"; ?>
								<a href="pets_product_page2.php?Prod_ID=<?php echo $_SESSION['Prod_ID']?>&Price=<?php echo $_SESSION['Price']?>&Quantity=<?php echo $qty+1?>&customer_ID=<?php echo $customer_ID?>"><input type="button" class="button" value="+"></a>
								<?php
								if ($_SESSION['Quantity']==0)
								{ ?>
									<?php
									$_SESSION['Quantity']=null;
								}
								elseif($_SESSION['Quantity']>1)
								{ ?>
									<a href="pets_product_page2.php?Prod_ID=<?php echo $_SESSION['Prod_ID']?>&Price=<?php echo $_SESSION['Price']?>&Quantity=<?php echo $_SESSION['Quantity']-1?>&customer_ID=<?php echo $customer_ID?>"><input type="button" class="button" value="-"></a></td></tr>
									<tr><td><button class="button" style="width: 107px;"><a href="../cart_with_css.php?Prod_ID=<?php echo $_SESSION['Prod_ID']?>&Quantity=<?php echo $_SESSION['Quantity']?>&customer_ID=<?php echo $customer_ID?>&c=0" style="text-decoration: none; color:black; position: relative; top:-10px; left:0px;"/><img src="css/images/cart.png" width="20" height="25" style="position: relative; top:9px; left:0px;"/> Add to Cart</a></button></td>

									<td><button class="button"><a href="../wishlist.php?Prod_ID=<?php echo $_SESSION['Prod_ID']?>&Quantity=<?php echo $_SESSION['Quantity']?>&customer_ID=<?php echo $customer_ID?>&w=1" style="text-decoration: none; color:black;"/><img src="css/images/wishlist.png" width="15" height="12"> Add to Wishlist</a></button></td><?php
								}
								else
								{
									?><tr><td><button class="button" style="width: 107px;"><a href="../cart_with_css.php?Prod_ID=<?php echo $_SESSION['Prod_ID']?>&Quantity=<?php echo $_SESSION['Quantity']?>&customer_ID=<?php echo $customer_ID?>&c=0" style="text-decoration: none; color:black; position: relative; top:-10px; left:0px;"/><img src="css/images/cart.png" width="20" height="25" style="position: relative; top:9px; left:0px;"/> Add to Cart</a></button></td>
									<td><button class="button"><a href="../wishlist.php?Prod_ID=<?php echo $_SESSION['Prod_ID']?>&Quantity=<?php echo $_SESSION['Quantity']?>&customer_ID=<?php echo $customer_ID?>&w=1" style="text-decoration: none; color:black;"/><img src="css/images/wishlist.png" width="15" height="12"> Add to Wishlist</a></button></td>
									<?php
								}
							}
							
							/*echo "<td><input type='number' name='quantity' style='width:50px'/></td>";/*
							echo '<input type="hidden" name="hidden_name" value="$Prod_Name"/>';
							echo '<input type="hidden" name="hidden_price" value="$Price"/>';
							echo '<input type="hidden" name="hidden_quantity" value="$quantity"/>';*/
							//$_SESSION['quantity']=1;
							?>
							<!--<td><button  type="submit"><a href="product_page2.php?Prod_ID=<?php echo $_SESSION["Prod_ID"]?>&Prod_Name=<?php echo $_SESSION['Prod_Name'] ?>&Price=<?php echo $_SESSION["Price"]?>&Quantity=<?php echo $_SESSION['Quantity']?>">+</a></button></td>-->

							<?php /*END OF QUANTITY AND ADD TO CART LINK*/ ?>
							<!--<input type="submit" value="Add to Cart" onclick="location='..\cart.php?Prod_ID=<?php echo $_SESSION['Prod_ID']?>&Quantity=<?php echo $_POST['hidden_quantity']?>"/>-->
							<!--Sections.php?Course_Code=<?php echo $_SESSION['Course_Code']?>&section=<?php echo $_SESSION['section']?>-->
							<?php
							//<!--<td><input type="submit" name="add" value="Add to Cart"/></td>--><?php
							//echo "</tr>"; 	
						}
						echo "</table>";
						echo "</form>";
					}	
				?>
			</div>
		</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="../footer.png" height="80px" width="200px"></br>
				<a href="../customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="../terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="../privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="../1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="../2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="../3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="../4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="../5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="../6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="../7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="../8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="../9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="../10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="../11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="../cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="../fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="../logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	</div>
</body>
</html>