<?php
	session_start();
	require 'dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="css/cart.css">
	<link rel="icon" href="ace_logo2.png" type="image/icon type">
	<title>Ace Hardware</title>
	<meta name="description" content="Write some words to describe your html page">
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="newhome.php"><img src="ace2.png" width="14%"></a>
				<div id="Login_Container1">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['customer_ID']))
							{
								echo "Welcome Guest!";?>
								<br><a href="../index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['customer_ID']))
							{
								echo "Welcome<br><font color='red'><b>".$_SESSION['fullname']."</b></font>!";
								echo "<form action='../logout.php' method='POST'><input type='submit' value='Logout' class='logout'/></form>";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
				<div id="Search">
					<form action="search.php" method="GET">
						<div id="Search_Content">
							<input type="text" id="search_box" name="search" placeholder="Search Product">
							<button type="submit" id="search_btn"><img src="css/images/search3.png" width="20px"></button>
						</div>
					</form>
				</div>
			</div>
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="dropdown">
				  <button class="dropbtn">Categories</button>
				  <div class="dropdown-content">
				    <a href="../1Paints_and_Sundries/paints_and_sundries.php">Paints and Sundries</a>
				    <a href="../2Tools/tools.php">Tools</a>
				    <a href="../3Electrical/electrical.php">Electrical</a>
				    <a href="../4Plumbing/plumbing.php">Plumbing</a>
				    <a href="../5Home_Hardware/home_hardware.php">Home Hardware</a>
				    <a href="../6Houseware/houseware.php">Houseware</a>
				    <a href="../7Lawn/lawn.php">Lawn and Outdoor</a>
				    <a href="../8Automotive/automotive.php">Automotive</a>
				    <a href="../9Appliances/appliances.php">Small Appliances</a>
				    <a href="../10Chemicals/chemicals.php">Chemicals and Batteries</a>
				    <a href="../11Pets/pets.php">Pets</a>
				  </div>
				</div>
				<div class="tab">
				  <a href="sale_and_specials.php"><button class="tablinks">Sale & Specials</button></a>
				  <button class="tablinks" onclick="location.href='../wishlist.php?customer_ID=<?php echo $_SESSION["customer_ID"]?>&w=0'">Wishlist</button>
				  <a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><button class="tablinks">Careers</button></a>
				  <a href="about_us.php"><button class="tablinks">About Us</button></a>
				</div>
				<div class="cart_tab">
					<?php
						if(empty($_SESSION['customer_ID']))
						{
							echo "<p><a href='cart_with_css.php'><font color='#FFFFFF'>Cart: 0 Item(s)</a></font></p>";
						}
						elseif(!empty($_SESSION['customer_ID']))
						{
							$select_cart="SELECT * FROM cart WHERE customer_ID='$_SESSION[customer_ID]'";
							$result=mysqli_query($connect,$select_cart);
							$cart_count=mysqli_num_rows($result);
						
							echo "<p><a href='cart_with_css.php'><font color='#FFFFFF'>Cart: ".$cart_count." Item(s)</a></font></p>"; 
						}
						else
						{
							echo ' ';
						}   
					?>
				</div>
			</div>
		</div>
		<div id="Center"> <!--category links-->
			<div class="prod_content">
				<div class="customer-service">
					<table>
						<form action="customer_service.php" method="POST">
							<tr>
								<td>
									<h1>Contact Us</h1>
									<label style='position:absolute;left:300px;top:100px;'>Concern</label></br>
									<select name="concern" style="position:absolute;left:300px;top:120px; width: 40%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;border-radius: 4px;box-sizing: border-box;">
										<option value="concern">-- Select a Concern --</option>
										<option value="delivery">Questions on Delivery</option>
										<option value="product">Questions on Product</option>
										<option value="return">Questions on Return</option>
										<option value="general">General Questions</option>
									</select>
									<label style="position:absolute;left:300px;top:200px;">Email</label></br>
									<input type="email" name="email" placeholder="Email" style="position:absolute;left:300px;top:220px;width: 40%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;border-radius: 4px;box-sizing: border-box;"/>
									<label style="position:absolute;left:300px;top:300px;">Message</label></br>
									<textarea rows="5" name="message" placeholder="Message" style="position:absolute;left:300px;top:320px;width: 40%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;border-radius: 4px;box-sizing: border-box;"></textarea></br>
									<input style="position:absolute;left: 600px;top:450px; width: 10%;background-color: #0000FF;color: white;padding: 10px 10px;margin: 5px 0;border: none;border-radius: 4px;cursor: pointer;" type="submit" value="Send" name="submit">

									<!--image links-->
									<img style="position:absolute;left: 100px;top:550px;" height="20px" width="20px" src="messenger.png">
									<a href="https://www.messenger.com/t/AceHardwarePhilippines"><font style="position:absolute;left: 130px;top:550px;" size="3px">Messenger | </font></a>  
									<img style="position:absolute;left: 220px;top:550px;" height="20px" width="20px" src="mail.png">
									<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><font style="position:absolute;left: 250px;top:550px;" size="3px">Gmail | </font></a>
									<img style="position:absolute;left: 310px;top:550px;" height="20px" width="20px" src="ig.png">
									<a href="https://www.instagram.com/acehardware_ph/"><font style="position:absolute;left: 340px;top:550px;" size="3px">Instagram</font></a>

									<!-- Slideshow container -->
									<div class="slideshow-container" style="position:absolute;top:600px;left:50px">

									  <!-- Full-width images with number and caption text -->
										<div class="mySlides fade">
											<img src="north-luzon-1.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="north-luzon-2.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="ncr-1.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="ncr-2.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="ncr-3.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="ncr-4.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="south-luzon-1.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="south-luzon-2.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="south-luzon-3.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="visayas-1.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="visayas-2.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="visayas-3.jpg"style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="mindanao-1.jpg" style="width:900px;height:500px;">
										</div>

										<div class="mySlides fade">
											<img src="mindanao-2.jpg" style="width:900px;height:500px;">
										</div>
									</div>
									<br>

									<!-- The dots/circles -->
									<div>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
										<span class="dot" style="position:relative;left:310px;top:900px;"></span>
									</div>

									<script type="text/javascript">
										var slideIndex = 0;
										showSlides();

										function showSlides() {
										    var i;
										    var slides = document.getElementsByClassName("mySlides");
										    var dots = document.getElementsByClassName("dot");
										    for (i = 0; i < slides.length; i++) {
										       slides[i].style.display = "none";  
										    }
										    slideIndex++;
										    if (slideIndex > slides.length) {slideIndex = 1}    
										    for (i = 0; i < dots.length; i++) {
										        dots[i].className = dots[i].className.replace(" active", "");
										    }
										    slides[slideIndex-1].style.display = "block";  
										    dots[slideIndex-1].className += " active";
										    setTimeout(showSlides, 3000); 
										}
									</script>								
								</td>
							
								<?php
									if(isset($_POST['submit']))
									{
										$concern=$_POST['concern'];
										$message=$_POST['message'];
										$email=$_POST['email'];

										$query="SELECT * FROM customer_service WHERE customer_name='$_SESSION[fullname]'";
										$result=mysqli_query($connect,$query);

										if(mysqli_num_rows($result)>0)
										{
											$update="UPDATE customer_service SET message='$_POST[message]' WHERE customer_name='$_SESSION[fullname]'";
											$result=mysqli_query($connect,$update);

											if($update)
											{
												echo '
													<script type="text/javascript"> 
														alert("Your concern has been updated!!!"); 
													</script>';
											}
										}
										else
										{
											$sql="INSERT INTO customer_service (ID,concern,message,customer_name,email) VALUES ('','$concern','$message','$_SESSION[fullname]','$email')";
											$insert=mysqli_query($connect,$sql);

											if($insert)
											{
												echo '
													<script type="text/javascript"> 
														alert("Your concern has been submitted!!!"); 
													</script>';
											}
										}
									}	
								?>
							</tr>
						</form>
					</table>
				</div>
			</div>
		</div>
		</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="footer.png" height="80px" width="200px"></br>
				<a href="customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	</div>
</body>
</html>

