<?php
	session_start();
	require 'dbconfig/config.php';
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="css/cart.css">
	<link rel="icon" href="ace_logo2.png" type="image/icon type">
	<title>Ace Hardware</title>
	<meta name="description" content="Write some words to describe your html page">
</head>
<body class="preload">
	<div class="blended_grid">
		<div id="fixed_top">
			<div id="TopNav"> <!--logo, signin, search, customer service, others-->
				<a href="newhome.php"><img src="ace2.png" width="14%"></a>
				<div id="Login_Container1">
					<div id="Login_Content">
						<?php  
							if(empty($_SESSION['customer_ID']))
							{
								echo "Welcome Guest!";?>
								<br><a href="index.php"> Create account | Sign in</a><?php
							}
							elseif(!empty($_SESSION['customer_ID']))
							{
								echo "Welcome<br><font color='red'><b>".$_SESSION['fullname']."</b></font>!";
								echo "<form action='logout.php' method='POST'><input type='submit' value='Logout' class='logout'/></form>";
							}
							else
							{
								echo '<script type="text/javascript"> alert("Invalid Username or Password!") </script>';
							} 
						?>
					</div>
				</div>
				<div id="Search">
					<form action="search.php" method="GET">
						<div id="Search_Content">
							<input type="text" id="search_box" name="search" placeholder="Search Product">
							<button type="submit" id="search_btn"><img src="css/images/search3.png" width="20px"></button>
						</div>
					</form>
				</div>
			</div>
				<div id="CustomerService_btn">
					<button class="button"><a href="customer_service.php"/>CUSTOMER SERVICE</a></button>
				</div>
			<div id="MenuNav"> <!--shop, sale&specials, wishlist, careers, about us-->
				<div class="dropdown">
				  <button class="dropbtn">Categories</button>
				  <div class="dropdown-content">
				    <a href="1Paints_and_Sundries/paints_and_sundries.php">Paints and Sundries</a>
				    <a href="2Tools/tools.php">Tools</a>
				    <a href="3Electrical/electrical.php">Electrical</a>
				    <a href="4Plumbing/plumbing.php">Plumbing</a>
				    <a href="5Home_Hardware/home_hardware.php">Home Hardware</a>
				    <a href="6Houseware/houseware.php">Houseware</a>
				    <a href="7Lawn/lawn.php">Lawn and Outdoor</a>
				    <a href="8Automotive/automotive.php">Automotive</a>
				    <a href="9Appliances/appliances.php">Small Appliances</a>
				    <a href="10Chemicals/chemicals.php">Chemicals and Batteries</a>
				    <a href="11Pets/pets.php">Pets</a>
				  </div>
				</div>
				<div class="tab">
				  <a href="sale_and_specials.php"><button class="tablinks">Sale & Specials</button></a>
				  <button class="tablinks" onclick="location.href='../wishlist.php?customer_ID=<?php echo $_SESSION["customer_ID"]?>&w=0'">Wishlist</button>
				  <a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><button class="tablinks">Careers</button></a>
				  <a href="about_us.php"><button class="tablinks">About Us</button></a>
				</div>
				<div class="cart_tab">
					<?php
						if(empty($_SESSION['customer_ID']))
						{
							echo "<p>Cart: 0 Item(s)</p>"; 
						}
						elseif(!empty($_SESSION['customer_ID']))
						{
							$select_cart="SELECT * FROM cart WHERE customer_ID='$_SESSION[customer_ID]'";
							$result=mysqli_query($connect,$select_cart);
							$cart_count=mysqli_num_rows($result);
						
							echo "<p>Cart: ".$cart_count." Item(s)</p>"; 
						}
						else
						{
							echo ' ';
						}   
					?>
				</div>
			</div>
		</div>
		<div id="Middle"> <!--breadcrumb-->
			<!--<div class="breadcrumbs">
				<p>My Cart</p>
			</div>-->
		</div>
		<div id="Center"> <!--category links-->
			<div class="prod_content">
				<?php
					$Total=0;
					$Subtotal=0;

					$Prod_ID=$_SESSION['Prod_ID'];
					$Quantity=$_SESSION['Quantity'];
					$customer_ID=$_SESSION['customer_ID'];

					/*$Prod_ID=$_REQUEST['Prod_ID'];
					
				
					$Quantity=$_REQUEST['Quantity'];
					

					$customer_ID=$_REQUEST['customer_ID'];
					*/

					$select_inv="SELECT * FROM inventory WHERE Prod_ID='$_SESSION[Prod_ID]'";
					$result_inv=mysqli_query($connect,$select_inv);

					if(mysqli_num_rows($result_inv)>0)
					{
						while($row=mysqli_fetch_array($result_inv))
						{
							if($row['Prod_ID']==$_SESSION["Prod_ID"])
							{
								$Prod_ID=$row['Prod_ID'];
								$Prod_Name=$row['Prod_Name'];
								$Price=$row['Price'];
							}
						}
					}
					else
					{
						echo "Product not found.";
					}

					$ctr=0;
					$select_cart="SELECT * FROM cart WHERE Prod_ID='$_SESSION[Prod_ID]'";
					$result2=mysqli_query($connect,$select_cart);
					if(mysqli_num_rows($result2)>0)
					{
						while($row=mysqli_fetch_array($result2))
						{
							if($row['customer_ID']==$_SESSION['customer_ID'])
							{
								if($row['Prod_ID']==$_SESSION['Prod_ID'])
								{
									//echo "<br><br>Prod ID at Customer ID".$_SESSION['Prod_ID']."<br>".$_SESSION['customer_ID'];
									$update_qty="UPDATE cart SET Quantity ='$Quantity' WHERE Cart_ID='$row[Cart_ID]'";
									$result_qty=mysqli_query($connect,$update_qty);

									$Subtotal=$row['Price']*$Quantity;
									$Total+=$Subtotal;

									$update_total="UPDATE cart SET Total ='$Total' WHERE Cart_ID='$row[Cart_ID]'";
									$result_total=mysqli_query($connect,$update_total);

									//echo "update ba talaga";
									$ctr=1;
								}
							}
						}
						
						if($ctr!=1)
						{
							$Subtotal=$Price*$Quantity;
							$Total+=$Subtotal;
							//echo "elseeeee!";
							$insert_cart="INSERT INTO cart (Cart_ID, Prod_ID, Prod_Name, Price, Quantity, Total, customer_ID) VALUES (' ',$_SESSION[Prod_ID]', '$Prod_Name', '$Price', '$_SESSION[Quantity]', '$Total', '$_SESSION[customer_ID]')";
							$result_insert=mysqli_query($connect,$insert_cart);
						}
					}
					else
					{
						$Total=0;
						$Subtotal=$Price*$Quantity;
						$Total=$Total+$Subtotal;
						$inser_cart2="INSERT INTO cart (Cart_ID, Prod_ID, Prod_Name, Price, Quantity, Total, customer_ID) VALUES (' ','$_SESSION[Prod_ID]', '$Prod_Name', '$Price', '$_SESSION[Quantity]', '$Total', '$_SESSION[customer_ID]')";
						$result_insert2=mysqli_query($connect,$inser_cart2);
					}

					$select_cart="SELECT * FROM cart WHERE customer_ID LIKE '%$customer_ID%'";
					$result_select=mysqli_query($connect,$select_cart);

					$Grand_Total=0;
					echo "<table frame=box bordercolor='#8C001A' class='prod_list' style='width:750px; height: 358px;'>";
					echo "<tr><th colspan='5' class='title'>My Cart<br></th></tr>";
					echo "<tr class='title'><td>Product Name</td><td>Price</td><td>Quantity</td><td>Subtotal</td><td>Action</td></tr>";

					if(mysqli_num_rows($result_select)>0)
					{
						while($row=mysqli_fetch_array($result_select))
						{
							$Prod_Name=$row['Prod_Name'];
							$Price=$row['Price'];
							$Quantity=$row['Quantity'];
							$Cart_ID=$row['Cart_ID'];
							$Subtotal=$Quantity*$Price;
							$Total=$row['Total'];
							$Grand_Total=$Grand_Total+$Total;

							echo "<tr class='borderbottom'><td>".$Prod_Name."</td><td>₱".$Price."</td><td>x".$Quantity."</td><td>₱".$Subtotal."</td>";
							echo "<td><a href='deleteprod.php?Cart_ID=".$Cart_ID."&customer_ID=".$customer_ID."&c=1'/><input type='button' class='button' value='Remove Item'></a></td></tr>";
						}
						echo "<tr class='borderbottom1'><td colspan='3' align= 'center'><b>Total</b></td>";
						echo "<td>₱<b>".$Grand_Total."</b></td><td><a href='purchase.php?customer_ID=$customer_ID'/><input type='button' class='button' value='Check Out'></a></td></tr>";
						echo "<tr align='center'><td colspan='5'><a href='deleteprod.php?Cart_ID=".$Cart_ID."&customer_ID=".$customer_ID."&c=0'/><input type='button' class='button' value='Cancel Order'></a>&nbsp;&nbsp;<a href='newhome.php'><input type='button' class='button' value='Back to Homepage'></a></td></tr>";
						echo "</table>";
						echo "</form>";
					}
					else
					{
						?><tr class='no_border'><td colspan='5' align='center'>Cart is empty.</td></tr>
						<tr class='no_border'><td colspan='5' align='center'><a href='newhome.php'><input type='button' class='button' value='Back to Homepage'></a></td></tr>
						<?php
					}	
				?>
			</div>
		</div>
		<div id="Footer"> <!--about us, contact us, privacy policy, disclaimer-->
			<div>
				<img style="margin-top: 0px" src="footer.png" height="80px" width="200px"></br>
				<a href="customer_service.php"><font color="white" size="2px">Contact Us</font></a><br>
				<a href="https://acehardware.ph/locator"><font color="white" size="2px">Store Locator</font></a><br>
				<a href="https://www.linkedin.com/company/ace-hardware-philippines-inc-/"><font color="white" size="2px">Careers</font></a><br>
				<a href="terms_and_conditions.php"><font color="white" size="2px">Terms & Conditions</font></a><br>
				<a href="privacy_policy.php"><font color="white" size="2px">Privacy Policy</font></a>
			</div>
			<div>
				<font color="white" size="4px"><b>Departments</b></font></br>
				<a href="1Paints_and_Sundries/paints_and_sundries.php"><font color="white" size="2px">Paints and Sundries</font></a><br>
			    <a href="2Tools/tools.php"><font color="white" size="2px">Tools</font></a><br>
			    <a href="3Electrical/electrical.php"><font color="white" size="2px">Electrical</font></a><br>
			    <a href="4Plumbing/plumbing.php"><font color="white" size="2px">Plumbing</font></a><br>
			    <a href="5Home_Hardware/home_hardware.php"><font color="white" size="2px">Home Hardware</font></a><br>
			    <a href="6Houseware/houseware.php"><font color="white" size="2px">Houseware</font></a><br>
			    <a href="7Lawn/lawn.php"><font color="white" size="2px">Lawn and Outdoor</font></a><br>
			    <a href="8Automotive/automotive.php"><font color="white" size="2px">Automotive</font></a><br>
			    <a href="9Appliances/appliances.php"><font color="white" size="2px">Small Appliances</font></a><br>
			    <a href="10Chemicals/chemicals.php"><font color="white" size="2px">Chemicals and Batteries</font></a><br>
			    <a href="11Pets/pets.php"><font color="white" size="2px">Pets</font></a>
			</div>
			<div>
				<font color="white" weight="bold" size="4px"><b>Privileges</b></font></br>
				<font color="white" size="2px">
					Shop with your ACE Rewards Card / SMAC / Prestige to get special discounts and earn points
				</font></br>
				<img style="margin-top: 0px" src="cards.png" height="80px" width="200px"></br>
				<font color="white" weight="bold" size="4px">Connect with Us</font></br>
				<font color="white" size="2px">
					Want to get updates on discounts, promos, tips, and all things ACE? Follow us on our social media account!
				</font></br></br>
				<a href="https://www.facebook.com/AceHardwarePhilippines/"><img style="margin-top: 0px; padding: 2px;" src="fb-icon.png" height="20px" width="20px"></a>
				<a href="https://www.instagram.com/acehardware_ph/"><img style="margin-top: 0px; padding: 2px;" src="logo-ig.png" height="21px" width="21px"></a>
				<a href="https://www.messenger.com/t/AceHardwarePhilippines"><img style="margin-top: 0px; padding: 2px;" src="messenger.png" height="21px" width="21px"></a>
				<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=acehardware2008@gmail.com"><img style="margin-top: 0px; padding: 2px;" src="mail.png" height="23px" width="23px"></a></br></br>
				<font color="white" size="3px">
					<b>&copy; Ace Hardware | Designed by GL</b>
				</font>
			</div>
		</div>
	
	</div>
</body>
</html>